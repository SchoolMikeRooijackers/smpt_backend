package services;

import entity.Dronesession;
import java.util.ArrayList;
import java.util.List;
import models.PilotResponse;
import models.PilotSession;
import models.PilotSessionResponse;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import rest.ObjectResponse;

/**
 *
 * @author hrutgers
 */
@Ignore
public class SessionServiceTest {

    private SessionService ss;
    private ObjectResponse actual, response;
    private PilotSessionResponse psr;
    private PilotResponse pilot;
    private List<PilotSessionResponse> sessions;

    public SessionServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ss = new SessionService();
        response = new ObjectResponse();
        actual = new ObjectResponse();
        psr = new PilotSessionResponse();
        sessions = new ArrayList<>();
        pilot = new PilotResponse(2, "thien");

        psr.setDroneId(1);
        psr.setVideoFeedId(1);
        psr.setPilot(pilot);

        sessions.add(psr);

        psr = new PilotSessionResponse();
        psr.setDroneId(1);
        psr.setVideoFeedId(1);
        psr.setStartTime("2015-12-08 12:04:11");
        psr.setEndTime("2015-12-10 22:07:57");
        psr.setPilot(pilot);
        sessions.add(psr);
    }

    @After
    public void tearDown() {
    }

    /**
     *
     */
    @Test
    public void getPilotSessions() {
        actual.setObject(sessions);
        response = ss.getPilotSessions();

        ArrayList x = (ArrayList) response.getObject();
        ArrayList y = (ArrayList) actual.getObject();

        PilotSessionResponse object = (PilotSessionResponse) x.get(0);
        PilotSessionResponse expected = (PilotSessionResponse) y.get(0);

        assertEquals(object.getDroneId(), expected.getDroneId());
        assertEquals(object.getVideoFeedId(), expected.getVideoFeedId());

        assertEquals(object.getPilot().id, expected.getPilot().id);
        assertEquals(object.getPilot().name, expected.getPilot().name);
    }

    /**
     *
     */
    @Test
    public void getPilotSessionsByUserId() {
        actual.setObject(sessions);
        response = ss.getPilotSessionsByUserId(pilot.id);

        ArrayList x = (ArrayList) response.getObject();
        ArrayList y = (ArrayList) actual.getObject();

        PilotSessionResponse object = (PilotSessionResponse) x.get(0);
        PilotSessionResponse expected = (PilotSessionResponse) y.get(0);

        assertEquals(object.getDroneId(), expected.getDroneId());
        assertEquals(object.getVideoFeedId(), expected.getVideoFeedId());
    }

    /**
     *
     */
    @Test
    public void openSession() {
        ObjectResponse expected = new ObjectResponse();
        expected.addError(0);
        PilotSession ps = new PilotSession(2);
        response = ss.openSession(ps);
        Dronesession ds = (Dronesession) response.object;
        List<Integer> failure = new ArrayList<>();
        failure.add(202);//no available drones

        List<Integer> actualError = response.getErrors();
        if (!actualError.contains(0)) {
            assertEquals(actualError, failure);
        } else {
            assertEquals(actualError, expected.getErrors());
            closeSession(ds.getId());
        }
    }

    public void closeSession(int id) {
        ss.closeDroneSession(id);
    }
}
