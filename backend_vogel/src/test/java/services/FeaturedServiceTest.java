/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entity.Dronesession;
import entity.FeaturedLocation;
import java.util.List;
import models.FeaturedLocationModel;
import models.PilotSession;
import org.jboss.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import rest.ObjectResponse;

/**
 *
 * @author Yasin
 */
@Ignore
public class FeaturedServiceTest {
    
    private final static Logger LOG = Logger.getLogger(FeaturedServiceTest.class);

    private static final int DRONE_ID = 1;
    private static final int PILOT_USER_ID = 2;
    private static final int LIVE_DRONE_SESSION_ID = 52;
    private static final int FEATURED_DRONE_SESSION_ID = 53;
    private static final int RECENT_DRONE_SESSION_ID = 54;
    
    private final static int ERROR_CODE_NO_DRONE_AVAILABLE = 202;
    
    private final static int SUCCESS_CODE = 0;
    private final static int ERROR_CODE_NON_EXISTING_SESSION_ID = 204;

    private final static int DRONE_SESSION_ID = 34;
    private final static int NON_EXISTING_SESSION_ID = 1000;
    
    public FeaturedServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class FeaturedService.
     */
    @Test
    public void testCreate() {
        FeaturedLocationModel model = new FeaturedLocationModel("Eindhoven", "Strijp s", "www.eindhoven.nl/strijp-s.png", DRONE_SESSION_ID);
        
        FeaturedService service = new FeaturedService();
        
        ObjectResponse expResult = new ObjectResponse();
        expResult.addError(SUCCESS_CODE);
        ObjectResponse result = service.create(model);
        
        assertEquals(expResult.succes(), result.succes());
    }
    
    /**
     * Test of create method, of class FeaturedService.
     */
    @Test
    public void testCreateNonExistingDroneSession() {
        FeaturedLocationModel model = new FeaturedLocationModel("Eindhoven", "Strijp s", "www.eindhoven.nl/strijp-s.png", NON_EXISTING_SESSION_ID);
        
        FeaturedService service = new FeaturedService();
        
        ObjectResponse expResult = new ObjectResponse();
        expResult.addError(ERROR_CODE_NON_EXISTING_SESSION_ID);
        ObjectResponse result = service.create(model);
        
        assertEquals(expResult.getErrors(), result.getErrors());
    }

    /**
     * Test of findAll method, of class FeaturedService.
     */
    @Test
    @Ignore
    public void testFindAll() {
        FeaturedService service = new FeaturedService();
        
        ObjectResponse expResult = new ObjectResponse();
        expResult.addError(SUCCESS_CODE);
        
        ObjectResponse result = service.findAll();
        List<FeaturedLocation> featuredLocations = (List<FeaturedLocation>) result.object;
        
        assertEquals(expResult.getErrors(), result.getErrors());
        assertTrue(featuredLocations.size() >= 1);
    }

    /**
     * Test of all method, of class FeaturedService.
     */
    @Test
    @Ignore
    public void testAll() {
        FeaturedService service = new FeaturedService();
        
        SessionService sessionService = new SessionService();
        PilotSession pilotSession = new PilotSession(PILOT_USER_ID);
        if (pilotSession == null) {
            fail("Pilot not found.");
        }
        
        ObjectResponse response = sessionService.openSession(pilotSession);
        response.getErrors().contains(202);
        Dronesession droneSession = (Dronesession) response.object;
        if (droneSession == null) {
            fail("No available drone");
        }
        
        try {
            List<Dronesession> live = (List<Dronesession>) service.findLive().object;
            List<Dronesession> featured = (List<Dronesession>) service.findFeatured().object;
            List<Dronesession> recent = (List<Dronesession>) service.findRecent().object;

            Dronesession liveDroneSession = (Dronesession) sessionService.getDroneSession(LIVE_DRONE_SESSION_ID).object;
            Dronesession featuredDroneSession = (Dronesession) sessionService.getDroneSession(FEATURED_DRONE_SESSION_ID).object;
            Dronesession recentDroneSession = (Dronesession) sessionService.getDroneSession(RECENT_DRONE_SESSION_ID).object;

            assertTrue(live.contains(liveDroneSession));
            assertTrue(featured.contains(featuredDroneSession));
            assertTrue(recent.contains(recentDroneSession));
        }
        catch (Exception e) {
            fail(e.getMessage());
        }
    }
    
}
