/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entity.Log;
import java.util.List;
import org.hibernate.Session;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pepijndegoede
 */
public class LogServiceTest {
    
    public LogServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of LogError method, of class LogService.
     */
    @Test
    public void testLogError() {
        
        LogService ls = new LogService();
        
        ls.LogError("Error", "Test of logging function");
        
        Log l = ls.GetLog().get(0);
        
        assertEquals("Test of logging function", l.getError());          
    }
}
