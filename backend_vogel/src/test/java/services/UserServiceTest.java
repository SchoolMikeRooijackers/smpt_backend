package services;

import entity.User;
import java.util.Objects;
import models.Login;
import models.Register;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import rest.ObjectResponse;

/**
 *
 * @author mikerooijackers
 */
public class UserServiceTest {

    private static UserService us;

    private final String nameGoed = "mike";
    private final String nameFout = "sdasdasdasdasdasdasdasdasdasdasdasdasdasdasda";
    private final String nameFoutEmpty = "";
    private final String emailGoed = "test@test.nl";
    private final String emailGoedRegister = "register@test.nl";
    private final String emailFout = "test.test.nl";
    private final String emailFoutEmpty = "";
    private final String phoneGoed = "+31614020101";
    private final String phoneFout = "061407212";
    private final String phoneFoutEmpty = "";
    private final String genderGoed = "m";
    private final String genderFout = "M";
    private final String genderFoutEmpty = "";
    private final String streetGoed = "Rachelsmolen";
    private final String streetFout = "";
    private final int houseNumber = 1;
    private final int houseNumberFout = 0;
    private final String suffix = "";
    private final String suffixFoutEmpty = null;
    private final String suffixFout45 = "sdasdasdasdasdasdasdasdasdasdasdasdasdasdasda";
    private final String zipcodeGoed = "5616HR";
    private final String zipcodeFout = "0782HR";
    private final String zipcodeFoutEmpty = "";
    private final String cityGoed = "Eindhoven";
    private final String cityFout = "";
    private final String cityFout45 = "sdasdasdasdasdasdasdasdasdasdasdasdasdasdasda";
    private final String countryGoed = "NL";
    private final String countryFout = "";
    private final String countryFout45 = "sdasdasdasdasdasdasdasdasdasdasdasdasdasdasda";
    private final String usernameGoed = "ndthien";
    private final String usernameGoedRegister = "qwert";
    private final String usernameFout = "yo";
    private final String usernameFoutEmpty = "";
    private final String passwordGoed = "9e72321a186dc1a629db80d87cd90e7380eb10a7";
    private final String passwordFout = "1";
    private final String passwordFoutEmpty = "";

    public UserServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        us = new UserService();
    }

    @AfterClass
    public static void tearDownClass() {
        
        
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
    
    @Test
    public void testGetUser() {
        System.out.println("getUser");
        UserService service = new UserService();
        entity.User u = new User(2, 0, 0, "thien", "ndthien@live.nl", "0612345678", 'm', "9e72321a186dc1a629db80d87cd90e7380eb10a7", "ndthien");
        
        //0 - success
        ObjectResponse actual = new ObjectResponse();
        actual.addError(0);
        actual.setObject(u);
        
        service.getUser(2);
        
        ObjectResponse response = service.getUser(2);
        
        User res = (User) response.getObject();
        
        assertEquals(res.getId(), u.getId());
        assertEquals(res.getEmail(), u.getEmail());
        assertEquals(res.getName(), u.getName());
        assertEquals(res.getUsername(), u.getUsername());
    }

    /**
     * Test of loginUser method, of class UserService.
     */
    @Test
    public void testLoginUser102() {
        System.out.println("loginUser - 102 - no username");
        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Login login = null;

        //102 - no username
        login = new Login("", passwordGoed);
        actual = us.loginUser(login);
        response.addError(102);
        assertEquals(response.getErrors(), actual.getErrors());
    }

    /**
     * Test of loginUser method, of class UserService.
     */
    @Test
    public void testLoginUser103() {
        //103 - invalid username
        System.out.println("loginUser - 103 - invalid username");
        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Login login = null;

        login = new Login(usernameFout, passwordGoed);
        actual = us.loginUser(login);
        response.addError(103);
        assertEquals(response.getErrors(), actual.getErrors());
    }

    /**
     * Test of loginUser method, of class UserService.
     */
    @Test
    public void testLoginUser111() {
        System.out.println("loginUser - 111 - no password");
        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Login login = null;

        //111 - no password
        response = new ObjectResponse();
        login = new Login(usernameGoed, "");
        actual = us.loginUser(login);
        response.addError(111);
        assertEquals(response.getErrors(), actual.getErrors());
    }

    /**
     * Test of loginUser method, of class UserService.
     */
    @Test
    public void testLoginUser114() {
        System.out.println("loginUser - 114 - invalid password");
        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Login login = null;

        //114 - invalid password
        response = new ObjectResponse();
        login = new Login(usernameGoed, passwordFout);
        actual = us.loginUser(login);
        response.addError(114);
        assertEquals(response.getErrors(), actual.getErrors());
    }

    /**
     * Test of loginUser method, of class UserService.
     */
    @Test
    public void testLoginUser() {
        
        System.out.println("loginUser - 0 - success");
        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Login login = null;

        //102 - no username
        login = new Login(usernameGoed, passwordGoed);
        actual = us.loginUser(login);
        response.addError(0);
        assertEquals(response.getErrors(), actual.getErrors());
       
    }

    /**
     * Test of registerUser method, of class UserService.
     */
    @Test
    @Ignore
    public void testRegisterUser() {
        System.out.println("registerUser - 0 - success");

        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Register register = null;

        //0 - success
        register = new Register(nameGoed, emailGoedRegister, phoneGoed, genderGoed, streetGoed, houseNumber, suffix, zipcodeGoed, cityGoed, countryGoed, usernameGoedRegister, passwordGoed);
        actual = us.registerUser(register);

        int userId = ((User) actual.getObject()).getId();
        response.addError(0);
        System.out.println(actual.getObject());
        System.out.println(userId);
        System.out.println(actual.getErrors());
        if (Objects.equals(response.getErrors(), actual.getErrors())) {
            us.deleteUser(userId);
        }
        assertEquals(response.getErrors(), actual.getErrors());
    }

    /**
     * Test of registerUser method, of class UserService.
     */
    @Test
    public void testRegisterUser102() {
        System.out.println("registerUser - 102 - no username");

        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Register register = null;

        //102 - no username
        register = new Register(nameGoed, emailGoed, phoneGoed, genderGoed, streetGoed, houseNumber, suffix, zipcodeGoed, cityGoed, countryGoed, usernameFoutEmpty, passwordGoed);
        actual = us.registerUser(register);
        response.addError(102);
        assertEquals(response.getErrors(), actual.getErrors());
    }

    /**
     * Test of registerUser method, of class UserService.
     */
    @Test
    public void testRegisterUser103() {
        System.out.println("registerUser - 103 - invalid username");

        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Register register = null;

        //103 - invalid username
        register = new Register(nameGoed, emailGoed, phoneGoed, genderGoed, streetGoed, houseNumber, suffix, zipcodeGoed, cityGoed, countryGoed, usernameFout, passwordGoed);
        actual = us.registerUser(register);
        response.addError(103);
        assertEquals(response.getErrors(), actual.getErrors());
    }

    /**
     * Test of registerUser method, of class UserService.
     */ 
    @Test
    public void testRegisterUser104() {
        System.out.println("registerUser - 104 - no city");

        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Register register = null;

        //104 - no city
        register = new Register(nameGoed, emailGoed, phoneGoed, genderGoed, streetGoed, houseNumber, suffix, zipcodeGoed, cityFout, countryGoed, usernameGoed, passwordGoed);
        actual = us.registerUser(register);
        response.addError(104);
        assertEquals(response.getErrors(), actual.getErrors());
    }

    /**
     * Test of registerUser method, of class UserService.
     */
    @Test
    public void testRegisterUser105() {
        System.out.println("registerUser - 105 - invalid city");

        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Register register = null;

        //105 - invalid city
        register = new Register(nameGoed, emailGoed, phoneGoed, genderGoed, streetGoed, houseNumber, suffix, zipcodeGoed, cityFout45, countryGoed, usernameGoed, passwordGoed);
        actual = us.registerUser(register);
        response.addError(105);
        assertEquals(response.getErrors(), actual.getErrors());
    }

    /**
     * Test of registerUser method, of class UserService.
     */
    @Test
    public void testRegisterUser106() {
        System.out.println("registerUser - 106 - no country");

        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Register register = null;

        //106 - no country
        register = new Register(nameGoed, emailGoed, phoneGoed, genderGoed, streetGoed, houseNumber, suffix, zipcodeGoed, cityGoed, countryFout, usernameGoed, passwordGoed);
        actual = us.registerUser(register);
        response.addError(106);
        assertEquals(response.getErrors(), actual.getErrors());
    }

    /**
     * Test of registerUser method, of class UserService.
     */
    @Test
    public void testRegisterUser107() {
        System.out.println("registerUser - 107 - invalid country");

        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Register register = null;

        //107 - invalid country
        register = new Register(nameGoed, emailGoed, phoneGoed, genderGoed, streetGoed, houseNumber, suffix, zipcodeGoed, cityGoed, countryFout45, usernameGoed, passwordGoed);
        actual = us.registerUser(register);
        response.addError(107);
        assertEquals(response.getErrors(), actual.getErrors());
    }

    /**
     * Test of registerUser method, of class UserService.
     */
    @Test
    public void testRegisterUser108() {
        System.out.println("registerUser - 108 - no houseNumber");

        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Register register = null;

        //108 - no houseNumber
        register = new Register(nameGoed, emailGoed, phoneGoed, genderGoed, streetGoed, houseNumberFout, suffix, zipcodeGoed, cityGoed, countryGoed, usernameGoed, passwordGoed);
        actual = us.registerUser(register);
        response.addError(108);
        assertEquals(response.getErrors(), actual.getErrors());
    }

    /**
     * Test of registerUser method, of class UserService.
     */
    @Test
    public void testRegisterUser109() {
        System.out.println("registerUser - 109 - no suffix");

        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Register register = null;

        //109 - no suffix
        register = new Register(nameGoed, emailGoed, phoneGoed, genderGoed, streetGoed, houseNumber, suffixFoutEmpty, zipcodeGoed, cityGoed, countryGoed, usernameGoed, passwordGoed);
        actual = us.registerUser(register);
        response.addError(109);
        assertEquals(response.getErrors(), actual.getErrors());
    }

    /**
     * Test of registerUser method, of class UserService.
     */
    @Test
    public void testRegisterUser110() {
        System.out.println("registerUser - 110 - invalid suffix");

        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Register register = null;

        //110 - invalid suffix
        register = new Register(nameGoed, emailGoed, phoneGoed, genderGoed, streetGoed, houseNumber, suffixFout45, zipcodeGoed, cityGoed, countryGoed, usernameGoed, passwordGoed);
        actual = us.registerUser(register);
        response.addError(110);
        assertEquals(response.getErrors(), actual.getErrors());
    }

    /**
     * Test of registerUser method, of class UserService.
     */
    @Test
    public void testRegisterUser111() {
        System.out.println("registerUser - 111 - no password");

        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Register register = null;

        //111 - no password
        register = new Register(nameGoed, emailGoed, phoneGoed, genderGoed, streetGoed, houseNumber, suffix, zipcodeGoed, cityGoed, countryGoed, usernameGoed, passwordFoutEmpty);
        actual = us.registerUser(register);
        response.addError(111);
        assertEquals(response.getErrors(), actual.getErrors());
    }

    /**
     * Test of registerUser method, of class UserService.
     */
    @Test
    public void testRegisterUser112() {
        System.out.println("registerUser - 112 - no street");

        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Register register = null;

        //112 - no street
        register = new Register(nameGoed, emailGoed, phoneGoed, genderGoed, streetFout, houseNumber, suffix, zipcodeGoed, cityGoed, countryGoed, usernameGoed, passwordGoed);
        actual = us.registerUser(register);
        response.addError(112);
        assertEquals(response.getErrors(), actual.getErrors());
    }

    /**
     * Test of registerUser method, of class UserService.
     */
    @Test
    public void testRegisterUser115() {
        System.out.println("registerUser - 115 - incorrect gender");

        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Register register = null;

        //115 - incorrect gender
        register = new Register(nameGoed, emailGoed, phoneGoed, genderFout, streetGoed, houseNumber, suffix, zipcodeGoed, cityGoed, countryGoed, usernameGoed, passwordGoed);
        actual = us.registerUser(register);
        response.addError(115);
        assertEquals(response.getErrors(), actual.getErrors());
    }

    /**
     * Test of registerUser method, of class UserService.
     */
    @Test
    public void testRegisterUser116() {
        System.out.println("registerUser - 116 - no gender");

        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Register register = null;

        //116 - no gender
        register = new Register(nameGoed, emailGoed, phoneGoed, genderFoutEmpty, streetGoed, houseNumber, suffix, zipcodeGoed, cityGoed, countryGoed, usernameGoed, passwordGoed);
        actual = us.registerUser(register);
        response.addError(116);
        assertEquals(response.getErrors(), actual.getErrors());
    }

    /**
     * Test of registerUser method, of class UserService.
     */
    @Test
    public void testRegisterUser117() {
        System.out.println("registerUser - 117 - invalid name");

        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Register register = null;

        //117 - invalid name
        register = new Register(nameFout, emailGoed, phoneGoed, genderGoed, streetGoed, houseNumber, suffix, zipcodeGoed, cityGoed, countryGoed, usernameGoed, passwordGoed);
        actual = us.registerUser(register);
        response.addError(117);
        assertEquals(response.getErrors(), actual.getErrors());
    }

    /**
     * Test of registerUser method, of class UserService.
     */
    @Test
    public void testRegisterUser118() {
        System.out.println("registerUser - 118 - no name");

        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Register register = null;

        //118 - no name
        register = new Register(nameFoutEmpty, emailGoed, phoneGoed, genderGoed, streetGoed, houseNumber, suffix, zipcodeGoed, cityGoed, countryGoed, usernameGoed, passwordGoed);
        actual = us.registerUser(register);
        response.addError(118);
        assertEquals(response.getErrors(), actual.getErrors());
    }

    /**
     * Test of registerUser method, of class UserService.
     */
    @Test
    public void testRegisterUser119() {
        System.out.println("registerUser - 119 - invalid email");

        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Register register = null;

        //119 - invalid email
        register = new Register(nameGoed, emailFout, phoneGoed, genderGoed, streetGoed, houseNumber, suffix, zipcodeGoed, cityGoed, countryGoed, usernameGoed, passwordGoed);
        actual = us.registerUser(register);
        response.addError(119);
        assertEquals(response.getErrors(), actual.getErrors());
    }

    /**
     * Test of registerUser method, of class UserService.
     */
    @Test
    public void testRegisterUser120() {
        System.out.println("registerUser - 120 - no email");

        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Register register = null;

        //120 - no email
        register = new Register(nameGoed, emailFoutEmpty, phoneGoed, genderGoed, streetGoed, houseNumber, suffix, zipcodeGoed, cityGoed, countryGoed, usernameGoed, passwordGoed);
        actual = us.registerUser(register);
        response.addError(120);
        assertEquals(response.getErrors(), actual.getErrors());
    }

    /**
     * Test of registerUser method, of class UserService.
     */
    @Test
    public void testRegisterUser121() {
        System.out.println("registerUser - 121 - invalid zipcode");

        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Register register = null;

        //121 = invalid zipcode
        register = new Register(nameGoed, emailGoed, phoneGoed, genderGoed, streetGoed, houseNumber, suffix, zipcodeFout, cityGoed, countryGoed, usernameGoed, passwordGoed);
        actual = us.registerUser(register);
        response.addError(121);
        assertEquals(response.getErrors(), actual.getErrors());
    }

    /**
     * Test of registerUser method, of class UserService.
     */
    @Test
    public void testRegisterUser122() {
        System.out.println("registerUser - 122 - no zipcode");

        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Register register = null;

        //122 - no zipcode
        register = new Register(nameGoed, emailGoed, phoneGoed, genderGoed, streetGoed, houseNumber, suffix, zipcodeFoutEmpty, cityGoed, countryGoed, usernameGoed, passwordGoed);
        actual = us.registerUser(register);
        response.addError(122);
        assertEquals(response.getErrors(), actual.getErrors());
    }

    /**
     * Test of registerUser method, of class UserService.
     */
    @Test
    public void testRegisterUser123() {
        System.out.println("registerUser - 123 - invalid phone");

        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Register register = null;

        //123 - invalid phone
        register = new Register(nameGoed, emailGoed, phoneFout, genderGoed, streetGoed, houseNumber, suffix, zipcodeGoed, cityGoed, countryGoed, usernameGoed, passwordGoed);
        actual = us.registerUser(register);
        response.addError(123);
        assertEquals(response.getErrors(), actual.getErrors());
    }

    /**
     * Test of registerUser method, of class UserService.
     */
    @Test
    public void testRegisterUser124() {
        System.out.println("registerUser - 124 - no phone");

        ObjectResponse response = new ObjectResponse();
        ObjectResponse actual = new ObjectResponse();
        Register register = null;

        //124 - no phone
        register = new Register(nameGoed, emailGoed, phoneFoutEmpty, genderGoed, streetGoed, houseNumber, suffix, zipcodeGoed, cityGoed, countryGoed, usernameGoed, passwordGoed);
        actual = us.registerUser(register);
        response.addError(124);
        assertEquals(response.getErrors(), actual.getErrors());
    }

}
