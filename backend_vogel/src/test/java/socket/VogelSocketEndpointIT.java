package socket;

import entity.Dronesession;
import entity.Videofeed;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.websocket.ClientEndpointConfig;
import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;
import org.glassfish.tyrus.test.tools.TestContainer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import socket.messages.response.ActivePilotsResponseMessage;

/**
 *
 * @author hrutgers
 */
public class VogelSocketEndpointIT extends TestContainer {

    // The container for the WebSocket
    private WebSocketContainer container;

    // An instance of the Socket EndPoint
    private VogelSocketEndpoint endpoint;

    // Endpoint config
    private ClientEndpointConfig config;

    // Session
    private Session session;

    // Websocket URL settings
    private static final String IP = "80.56.241.100";
    private static final String PORT = "8080";
    private static final int USER_ID = 1;
    private static final String SNAPSHOT = "backend_vogel-1.0-SNAPSHOT";

    // URL path for the socket
    private static String CONTEXT_PATH;

    public VogelSocketEndpointIT() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws URISyntaxException, UnknownHostException, DeploymentException, IOException {
        // Get the localhost IP
        InetAddress localhost = InetAddress.getLocalHost();

        // Url for the socket "ws://" + IP + ":" + PORT + "/" + SNAPSHOT + "/socket";
        CONTEXT_PATH = "ws://" + localhost.getHostAddress() + ":" + PORT + "/" + SNAPSHOT + "/socket";

        this.container = ContainerProvider.getWebSocketContainer();
        this.endpoint = new VogelSocketEndpoint();
        this.session = this.container.connectToServer(this.endpoint, new URI(CONTEXT_PATH));
        this.config = ClientEndpointConfig.Builder.create().build();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of onError method, of class VogelSocketEndpoint.
     */
    @Test
    public void testOnError() {
        try {
            this.endpoint.onError(this.session, new Throwable());
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    /**
     * Test of onOpen method, of class VogelSocketEndpoint.
     */
    @Test
    public void testOnOpen() {
        try {
            this.endpoint.onOpen(this.session, this.config);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    /**
     * Test of onClose method, of class VogelSocketEndpoint.
     */
    @Test
    public void testOnClose() throws Exception {
        try {
            this.endpoint.onClose(this.session);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    /**
     * Test of onMessage method, of class VogelSocketEndpoint. This method can't
     * be tested by an unit test alone so this test will always succeed.
     */
    @Test
    public void testOnMessage() {
        System.out.println("onMessage - This method can't be tested by an unit test alone");
        assertTrue(true);
    }

    /**
     * Test of sendMessageToClient method, of class VogelSocketEndpoint.
     */
    @Test
    public void testSendMessageToClient() {
        try {
            List<Dronesession> activePilotSessions = new ArrayList<>();
            entity.Location currLoc1 = new entity.Location(1);
            currLoc1.setLatitude(55.3F);
            currLoc1.setLongitude(20.2F);
            currLoc1.setAltitude(500.5F);
            entity.Location baseLoc1 = new entity.Location(2);
            baseLoc1.setLatitude(44.3F);
            baseLoc1.setLongitude(5.2F);
            baseLoc1.setAltitude(340.5F);
            entity.Drone d1 = new entity.Drone(1);
            d1.setCurrentLocationId(currLoc1);
            d1.setBaseLocationId(baseLoc1);
            Videofeed vf1 = new Videofeed(1);
            vf1.setStreamURL("https://thestreamurl.com");
            vf1.setFileLocation("https://thefilelocation.com");
            Dronesession ds1 = new Dronesession();
            ds1.setDroneId(d1);
            ds1.setVideoFeedId(vf1);
            ds1.setStartTime(new Date());
            ds1.setEndTime(new Date());
            activePilotSessions.add(ds1);
            this.endpoint.sendMessageToClient(this.session, new ActivePilotsResponseMessage(activePilotSessions));
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    /**
     * Test of sendErrorToClient method, of class VogelSocketEndpoint.
     */
    @Test
    public void testSendErrorToClient() {
        try {
            this.endpoint.sendErrorToClient(this.session, 300);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
}
