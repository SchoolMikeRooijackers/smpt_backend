/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author pepijndegoede
 */
public class LogTest {
    
    Log log;
    
    public LogTest() {
        //log = new Log(3, new Date("12-12-2015"), "123", "error");
        log = new Log(3);
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getId method, of class Log.
     */
    @Test
    public void testGetId() {
        System.out.println("GetId");
        Integer expResult = 3;
        Integer result = log.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDate method, of class Log.
     */
    @Test
    public void testGetDate() {
        System.out.println("getDate");
        Log instance = new Log();
        Date expResult = null;
        Date result = instance.getDate();
        assertEquals(expResult, result);
    }

    /**
     * Test of getErrorCode method, of class Log.
     */
    @Test
    public void testGetErrorCode() {
        System.out.println("getErrorCode");
        Log instance = new Log();
        String expResult = null;
        String result = instance.getErrorCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of getError method, of class Log.
     */
    @Test
    public void testGetError() {
        System.out.println("getError");
        Log instance = new Log();
        String expResult = null;
        String result = instance.getError();
        assertEquals(expResult, result);
    }

}
