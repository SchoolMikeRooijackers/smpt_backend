/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author mikerooijackers
 */
public class AddressTest {
    
    public AddressTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getId method, of class Address.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        Address instance = new Address();
        Integer expResult = null;
        Integer result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getStreet method, of class Address.
     */
    @Test
    public void testGetStreet() {
        System.out.println("getStreet");
        Address instance = new Address();
        String expResult = null;
        String result = instance.getStreet();
        assertEquals(expResult, result);
    }

    /**
     * Test of getHouseNumber method, of class Address.
     */
    @Test
    public void testGetHouseNumber() {
        System.out.println("getHouseNumber");
        Address instance = new Address();
        int expResult = 0;
        int result = instance.getHouseNumber();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSuffix method, of class Address.
     */
    @Test
    public void testGetSuffix() {
        System.out.println("getSuffix");
        Address instance = new Address();
        String expResult = null;
        String result = instance.getSuffix();
        assertEquals(expResult, result);
    }

    /**
     * Test of getZipcode method, of class Address.
     */
    @Test
    public void testGetZipcode() {
        System.out.println("getZipcode");
        Address instance = new Address();
        String expResult = null;
        String result = instance.getZipcode();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCity method, of class Address.
     */
    @Test
    public void testGetCity() {
        System.out.println("getCity");
        Address instance = new Address();
        String expResult = null;
        String result = instance.getCity();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCountry method, of class Address.
     */
    @Test
    public void testGetCountry() {
        System.out.println("getCountry");
        Address instance = new Address();
        String expResult = null;
        String result = instance.getCountry();
        assertEquals(expResult, result);
    }

    /**
     * Test of getUserList method, of class Address.
     */
    @Test
    public void testGetUserList() {
        System.out.println("getUserList");
        Address instance = new Address();
        List<User> expResult = null;
        List<User> result = instance.getUserList();
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class Address.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Address instance = new Address();
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Address.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object object = null;
        Address instance = new Address();
        boolean expResult = false;
        boolean result = instance.equals(object);
        assertEquals(expResult, result);
    }

}
