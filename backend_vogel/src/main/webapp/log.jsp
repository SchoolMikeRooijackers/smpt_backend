<%-- 
    Document   : test
    Created on : Dec 8, 2015, 12:11:13 PM
    Author     : pepijndegoede
--%>

<%@page import="entity.Log"%>
<%@page import="java.util.List"%>
<%@page import="services.LogService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Log</title>
        <style>
            table {
                margin: 0px;
                padding: 0px;
                width: 100%;
                border-collapse: collapse;
            }
            
            th, td {
                border: 1px solid black;
                padding: 4px;
            }
        </style>
    </head>
    <body>
        <h1>Hello World!</h1>
        <table>
            <tr>
                <th style="width: 15%;">
                    Date
                </th>
                <th style="width: 15%;">
                    Error code
                </th>
                <th style="width: 70%;">
                    Error
                </th>
                <%
                    LogService ls = new LogService();

                    List<Log> logs = ls.GetLog();

                    for (Log l : logs) {
                        out.print("<tr><td>" + l.getDate() + "</td>");
                        out.print("<td>" + l.getErrorCode() + "</td>");
                        out.print("<td>" + l.getError() + "</td></tr>");
                    }
                %>
            </tr>
        </table>        
    </body>
</html>
