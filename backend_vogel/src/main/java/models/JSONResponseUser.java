/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import entity.User;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pepijndegoede
 */
public class JSONResponseUser {
    
        /**
     *
     */
    public List<Integer> errors;

    /**
     *
     */
    public User user;

    /**
     *
     */
    public JSONResponseUser() {
        this.errors = new ArrayList<>();
    }
}
