package models;

/**
 *
 * @author hrutgers
 */
public class PilotSessionResponse {
    private Integer droneId;
    private Integer videoFeedId;
    private String startTime;
    private String endTime;
    private PilotResponse pilot;

    /**
     *
     * @return
     */
    public Integer getDroneId() {
        return droneId;
    }

    /**
     *
     * @param droneId
     */
    public void setDroneId(Integer droneId) {
        this.droneId = droneId;
    }

    /**
     *
     * @return
     */
    public Integer getVideoFeedId() {
        return videoFeedId;
    }

    /**
     *
     * @param videoFeedId
     */
    public void setVideoFeedId(Integer videoFeedId) {
        this.videoFeedId = videoFeedId;
    }

    /**
     *
     * @return
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     *
     * @param startTime
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     *
     * @return
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     *
     * @param endTime
     */
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    /**
     *
     * @return
     */
    public PilotResponse getPilot() {
        return pilot;
    }

    /**
     *
     * @param pilot
     */
    public void setPilot(PilotResponse pilot) {
        this.pilot = pilot;
    }
      
    /**
     *
     */
    public PilotSessionResponse(){
    }
    
    /**
     *
     * @param droneId
     * @param videoFeedId
     * @param startTime
     * @param endTime
     */
    public PilotSessionResponse(Integer droneId, Integer videoFeedId, String startTime, String endTime) {
        this.droneId = droneId;
        this.videoFeedId = videoFeedId;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    /**
     *
     * @param droneId
     * @param videoFeedId
     * @param startTime
     * @param endTime
     * @param pilot
     */
    public PilotSessionResponse(int droneId, int videoFeedId, String startTime, String endTime, PilotResponse pilot) {
        this.droneId = droneId;
        this.videoFeedId = videoFeedId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.pilot = pilot;
    }

    /**
     *
     * @param droneId
     * @param videoFeedId
     * @param startTime
     * @param pilot
     */
    public PilotSessionResponse(int droneId, int videoFeedId, String startTime, PilotResponse pilot) {
        this.droneId = droneId;
        this.videoFeedId = videoFeedId;
        this.startTime = startTime;
        this.pilot = pilot;
    }

    /**
     *
     * @param droneId
     * @param videoFeedId
     * @param startTime
     */
    public PilotSessionResponse(int droneId, int videoFeedId, String startTime) {
        this.droneId = droneId;
        this.videoFeedId = videoFeedId;
        this.startTime = startTime;
        this.pilot = pilot;
    }
}
