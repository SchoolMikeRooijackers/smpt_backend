package models;

import entity.Dronesession;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Yasin
 */
public class FeaturedLocationModel {
    
    private String title;
    private String subTitle;
    private String imageUrl;
    private int droneSessionId;
    
    private List<Dronesession> live = new ArrayList<>();
    private List<Dronesession> recent = new ArrayList<>();
    private List<Dronesession> featured = new ArrayList<>();
    
    /**
     *
     */
    public FeaturedLocationModel() {
        
    }

    /**
     *
     * @param title
     * @param subTitle
     * @param imageUrl
     */
    public FeaturedLocationModel(String title, String subTitle, String imageUrl) {
        this.title = title;
        this.subTitle = subTitle;
        this.imageUrl = imageUrl;
    }
    
    /**
     * 
     * @param title
     * @param subTitle
     * @param imageUrl
     * @param droneSessionId 
     */
    public FeaturedLocationModel(String title, String subTitle, String imageUrl, int droneSessionId) {
        this.title = title;
        this.subTitle = subTitle;
        this.imageUrl = imageUrl;
        this.droneSessionId = droneSessionId;
    }
    
    /**
     *
     * @param id
     */
    public void setDroneSessionId(int id) {
        this.droneSessionId = id;
    }

    /**
     *
     * @param live
     */
    public void setLive(List<Dronesession> live) {
        this.live = live;
    }

    /**
     *
     * @param recent
     */
    public void setRecent(List<Dronesession> recent) {
        this.recent = recent;
    }

    /**
     *
     * @param featured
     */
    public void setFeatured(List<Dronesession> featured) {
        this.featured = featured;
    }
    
    /**
     *
     * @return
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @return
     */
    public String getSubTitle() {
        return subTitle;
    }

    /**
     *
     * @return
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     *
     * @return
     */
    public int getDroneSessionId() {
        return droneSessionId;
    }
    
}
