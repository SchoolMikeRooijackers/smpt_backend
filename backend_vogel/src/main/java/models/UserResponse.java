package models;

import entity.User;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author thiennguyen
 */
public class UserResponse {

    private List<Integer> errors;
    private User user;

    /**
     *
     */
    public UserResponse() {
        this.errors = new ArrayList<>();
    }

    /**
     *
     * @return List errors
     */
    public List<Integer> getErrors() {
        return this.errors;
    }

    /**
     *
     * @param errors
     */
    public void setErrors(List<Integer> errors) {
        this.errors = errors;
    }

    /**
     *
     * @return User user
     */
    public User getUser() {
        return this.user;
    }

    /**
     *
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

}
