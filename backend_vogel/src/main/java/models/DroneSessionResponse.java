package models;

import entity.Dronesession;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Yasin
 */
public class DroneSessionResponse {
    
    private List<Integer> errors;

    /**
     *
     */
    public List<Dronesession> droneSessions;
    
    /**
     *
     */
    public DroneSessionResponse() {
        errors = new ArrayList<>();
    }
    
    /**
     *
     * @param errors
     */
    public void setErrors(List<Integer> errors) {
        this.errors = errors;
    }

    /**
     *
     * @return
     */
    public List<Integer> getErrors() {
        return this.errors;
    }
    
    /**
     *
     * @param list
     */
    public void setDroneSession(List<Dronesession> list) {
        this.droneSessions = list;
    }
    
}
