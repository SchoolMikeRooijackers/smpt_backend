/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author hrutgers
 */
public class PilotResponse {

    /**
     *
     */
    public Integer id;

    /**
     *
     */
    public String name;
    
    /**
     *
     */
    public PilotResponse(){}
    
    /**
     *
     * @param id
     * @param name
     */
    public PilotResponse(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
}
