/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.List;

/**
 *
 * @author thiennguyen
 */
public class UserDroneSessionResponse {

    private List<Integer> errors;
    private int droneSessionId;

    /**
     *
     * @return
     */
    public List<Integer> getErrors() {
        return errors;
    }

    /**
     *
     * @return
     */
    public int getDroneSessionId() {
        return droneSessionId;
    }

    /**
     *
     * @param errors
     */
    public void setErrors(List<Integer> errors) {
        this.errors = errors;
    }

    /**
     *
     * @param droneId
     */
    public void setDroneSessionId(int droneId) {
        this.droneSessionId = droneId;
    }

}
