package models;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pepijndegoede
 */
public class JSONResponse {

    /**
     *
     */
    public List<Integer> errors;

    /**
     *
     */
    public Integer id;

    /**
     *
     */
    public JSONResponse() {
        this.errors = new ArrayList<>();
    }
}
