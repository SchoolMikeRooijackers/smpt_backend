package models;

/**
 *
 * @author thiennguyen
 */
public class Login {

    /**
     *
     */
    public String username;

    /**
     *
     */
    public String password;

    /**
     *
     */
    public Login() {
    }

    /**
     *
     * @param username
     * @param password
     */
    public Login(String username, String password) {
        this.username = username;
        this.password = password;
    }

}
