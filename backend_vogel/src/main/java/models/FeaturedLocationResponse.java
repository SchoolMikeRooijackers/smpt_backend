package models;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Yasin
 */
public class FeaturedLocationResponse {
    
    private List<Integer> errors;

    /**
     *
     */
    public List<FeaturedLocationModel> featuredLocations;

    /**
     *
     */
    public FeaturedLocationResponse() {
        errors = new ArrayList<>();
    }

    /**
     *
     * @param list
     */
    public FeaturedLocationResponse(List<FeaturedLocationModel> list) {
        this.featuredLocations = list;
    }

    /**
     *
     * @param errors
     */
    public void setErrors(List<Integer> errors) {
        this.errors = errors;
    }

    /**
     *
     * @return
     */
    public List<Integer> getErrors() {
        return this.errors;
    }

}
