package models;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author thiennguyen
 */
public class LoginResponse {

    private List<Integer> errors;
    private int id;
    private String username;
    private String email;

    /**
     *
     */
    public LoginResponse() {
        this.errors = new ArrayList<>();
    }

    /**
     *
     * @return List errors
     */
    public List<Integer> getErrors() {
        return this.errors;
    }

    /**
     *
     * @param errors
     */
    public void setErrors(List<Integer> errors) {
        this.errors = errors;
    }

    /**
     *
     * @return int id
     */
    public int getId() {
        return this.id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return String username
     */
    public String getUsername() {
        return this.username;
    }

    /**
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return
     */
    public String getEmail() {
        return this.email;
    }

    /**
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

}
