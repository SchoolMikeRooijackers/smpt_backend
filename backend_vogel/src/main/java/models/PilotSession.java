/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author thiennguyen
 */
public class PilotSession {
    private int id;

    /**
     *
     */
    public PilotSession() {
    }

    /**
     *
     * @param id
     */
    public PilotSession(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }
    
}
