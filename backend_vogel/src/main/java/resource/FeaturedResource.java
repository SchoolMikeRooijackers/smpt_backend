package resource;

import entity.Dronesession;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import models.DroneSessionResponse;
import models.FeaturedLocationModel;
import models.FeaturedLocationResponse;
import models.JSONResponse;
import rest.ObjectResponse;
import services.FeaturedService;

/**
 *
 * @author Yasin
 */
@Path("featuredlocation")
public class FeaturedResource {

    private FeaturedService service;
    
    /**
     *
     */
    public FeaturedResource() {
        service = new FeaturedService();
    }
    
    /**
     *
     * @param model
     * @return
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(FeaturedLocationModel model) {
        ObjectResponse response = service.create(model);
 
        JSONResponse jsonResponse = new JSONResponse();

        jsonResponse.errors = response.getErrors();
        jsonResponse.id = (Integer) response.object;

        return Response.ok(jsonResponse).build();
    }
    
    /**
     *
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/all")
    public Response findAll() {
        ObjectResponse response = service.all(); // service.findAll();

        FeaturedLocationResponse jsonResponse = new FeaturedLocationResponse();

        jsonResponse.setErrors(response.getErrors());
        
        if (response.succes()) {
            jsonResponse.featuredLocations = (List<FeaturedLocationModel>) response.object; 
        }

        return Response.ok(jsonResponse).build();
    }
    
    /**
     *
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/live")
    public Response findLive() {
        ObjectResponse response = service.findLive();

        DroneSessionResponse jsonResponse = new DroneSessionResponse();

        jsonResponse.setErrors(response.getErrors());
        
        if (response.succes()) {
            jsonResponse.droneSessions = (List<Dronesession>) response.object; 
        }

        return Response.ok(jsonResponse).build();
    }
    
    /**
     *
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/recent")
    public Response findRecent() {
        ObjectResponse response = service.findRecent();

        DroneSessionResponse jsonResponse = new DroneSessionResponse();

        jsonResponse.setErrors(response.getErrors());
        
        if (response.succes()) {
            jsonResponse.droneSessions = (List<Dronesession>) response.object; 
        }

        return Response.ok(jsonResponse).build();
    }
    
    /**
     *
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/featured")
    public Response findFeatured() {
        ObjectResponse response = service.findFeatured();

        DroneSessionResponse jsonResponse = new DroneSessionResponse();

        jsonResponse.setErrors(response.getErrors());
        
        if (response.succes()) {
            jsonResponse.droneSessions = (List<Dronesession>) response.object; 
        }

        return Response.ok(jsonResponse).build();
    }
    
}
