package resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import models.JSONResponse;

/**
 * Resource for testing if there is a server connection
 *
 * @author thiennguyen
 */
@Path("test")
public class ConnectionResource {

    /**
     *
     * @return Response
     */
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response test() {
        JSONResponse json = new JSONResponse();
        json.errors.add(0);

        return Response.ok(json).build();
    }
}
