package resource;

import entity.User;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import models.JSONResponse;
import models.JSONResponseUser;
import models.Login;
import models.LoginResponse;
import models.Register;
import models.UserResponse;
import rest.ObjectResponse;
import services.UserService;

/**
 * REST Web Service
 *
 * @author pepijndegoede
 */
@Path("user")
public class UserResource {

    @Context
    private UriInfo context;
    private UserService service;

    /**
     * Creates a new instance of UserResource
     */
    public UserResource() {
        service = new UserService();
    }

    /**
     * Test
     * @return Response
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/registerTest")
    public Response get() {
        models.Register register = new models.Register("asd", "asd", "asd", "asd", "asd", 1, "asd", "asd", "asd", "asd", "asd", "asd");

        return Response.ok(register).build();
    }

    /**
     *
     * @param register
     * @return Response
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/register")
    public Response register(Register register) {
        ObjectResponse response = service.registerUser(register);
        JSONResponseUser jsonResponse = new JSONResponseUser();

        jsonResponse.errors = response.getErrors();
        
        jsonResponse.user = (User) response.object;
        
        return Response.ok(jsonResponse).build();
    }

    /**
     * Method for loggin in the user
     * @param l - Login object containing username and password
     * @return Response object containing a list of errors and, if available, an id, email and username.
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/login")
    public Response login(Login l) {
        try {
            ObjectResponse response = service.loginUser(l);
            JSONResponseUser login = new JSONResponseUser();
            login.errors = response.getErrors();
            if (response.succes()) {    
                login.user = (User) response.getObject();
            }
            return Response.ok(login).build();
        } catch (Exception e) {
            return Response.serverError().build();
        }
    }

    /**
     * Method for retrieving the user object.
     * @param userId
     * @return Response object containing a list of errors and, if available, the information of an user
     */
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{userId}")
    public Response getUser(@PathParam("userId") int userId) {
        try {
            ObjectResponse response = service.getUser(userId);

            UserResponse user = new UserResponse();
            user.setErrors(response.getErrors());
            user.setUser((User) response.getObject());

            return Response.ok(user).build();
        } catch (Exception e) {
            System.out.println("api/user/ - getUser - " + e.toString());
            return Response.serverError().build();
        }
    }

    /**
     * PUT method for updating or creating an instance of UserResource
     *
     * @param content representation for the resource
     */
    @PUT
    @Consumes("application/json")
    public void putJson(String content) {
    }
}
