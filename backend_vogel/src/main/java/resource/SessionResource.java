package resource;

import entity.Dronesession;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.PathParam;
import models.PilotSessionListResponse;
import models.PilotSessionResponse;
import rest.ObjectResponse;
import services.SessionService;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import models.PilotSession;
import models.UserDroneSessionResponse;

/**
 *
 * @author mikerooijackers
 */
@Path("sessions")
public class SessionResource {

    private final SessionService service;

    /**
     *
     */
    public SessionResource() {
        this.service = new SessionService();
    }

    /**
     * Return a list of all pilotsessions
     *
     * @return objectResponse Reponse-object containing ArrayList of
     * pilotsessions
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/pilot")
    public Response get() {
        try {
            ObjectResponse objectResponse = this.service.getPilotSessions();
            PilotSessionListResponse pilotSessionListResponse = new PilotSessionListResponse();
            pilotSessionListResponse.errors = objectResponse.getErrors();
            pilotSessionListResponse.sessions = (List<PilotSessionResponse>) objectResponse.getObject();
            return Response.ok(pilotSessionListResponse).build();
        } catch (Exception e) {
            System.out.println("*** api/sessions/pilot: " + e.toString());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * Returns a list of sessions of one pilot by user-id.
     *
     * @param userid int user-id of the pilot
     * @return objectResponse Reponse-object containing ArrayList pilotsessions
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/pilot/{userid}")
    public Response get(@PathParam("userid") int userid) {
        try {
            ObjectResponse objectResponse = this.service.getPilotSessionsByUserId(userid);
            PilotSessionListResponse pilotSessionListResponse = new PilotSessionListResponse();
            pilotSessionListResponse.errors = objectResponse.getErrors();
            pilotSessionListResponse.sessions = (List<PilotSessionResponse>) objectResponse.getObject();
            return Response.ok(pilotSessionListResponse).build();
        } catch (Exception e) {
            System.out.println("*** api/sessions/pilot/{userid}: " + e.toString());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * Method for checking if there is a available drone for the user to control.
     * @param ps - PilotSession object containing a userId
     * @return a response object containing a list with errors and, if available, a droneId whom the user can control
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/pilot/")
    public Response openSession(PilotSession ps) {
        try {
            ObjectResponse object = this.service.openSession(ps);
            UserDroneSessionResponse response = new UserDroneSessionResponse();
            response.setErrors(object.getErrors());
            if (object.succes()) {
                Dronesession ds = (Dronesession) object.getObject();
                response.setDroneSessionId(ds.getId());
            }
            return Response.ok(response).build();
        } catch (Exception e) {
            System.out.println("api/sessions/pilot - opensession - " + e.toString());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
