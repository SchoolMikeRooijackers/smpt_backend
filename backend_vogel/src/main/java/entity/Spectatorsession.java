/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mikerooijackers
 */
@Entity
@Table(name = "spectatorsession")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Spectatorsession.findAll", query = "SELECT s FROM Spectatorsession s"),
    @NamedQuery(name = "Spectatorsession.findById", query = "SELECT s FROM Spectatorsession s WHERE s.id = :id"),
    @NamedQuery(name = "Spectatorsession.findByStartTime", query = "SELECT s FROM Spectatorsession s WHERE s.startTime = :startTime"),
    @NamedQuery(name = "Spectatorsession.findByEndTime", query = "SELECT s FROM Spectatorsession s WHERE s.endTime = :endTime")})
public class Spectatorsession implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "startTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startTime;
    @Column(name = "endTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endTime;
    @JoinColumn(name = "dronesessionId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Dronesession dronesessionId;
    @JoinColumn(name = "spectatorId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private User spectatorId;

    public Spectatorsession() {
    }

    public Spectatorsession(Integer id) {
        this.id = id;
    }

    public Spectatorsession(Integer id, Date startTime) {
        this.id = id;
        this.startTime = startTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Dronesession getDronesessionId() {
        return dronesessionId;
    }

    public void setDronesessionId(Dronesession dronesessionId) {
        this.dronesessionId = dronesessionId;
    }

    public User getSpectatorId() {
        return spectatorId;
    }

    public void setSpectatorId(User spectatorId) {
        this.spectatorId = spectatorId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Spectatorsession)) {
            return false;
        }
        Spectatorsession other = (Spectatorsession) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Spectatorsession[ id=" + id + " ]";
    }
    
}
