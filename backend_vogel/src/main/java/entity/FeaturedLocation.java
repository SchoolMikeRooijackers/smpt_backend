/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mikerooijackers
 */
@Entity
@Table(name = "featured_location")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FeaturedLocation.findAll", query = "SELECT f FROM FeaturedLocation f"),
    @NamedQuery(name = "FeaturedLocation.findById", query = "SELECT f FROM FeaturedLocation f WHERE f.id = :id"),
    @NamedQuery(name = "FeaturedLocation.findByTitle", query = "SELECT f FROM FeaturedLocation f WHERE f.title = :title"),
    @NamedQuery(name = "FeaturedLocation.findBySubTitle", query = "SELECT f FROM FeaturedLocation f WHERE f.subTitle = :subTitle")})
public class FeaturedLocation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "title")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "subTitle")
    private String subTitle;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "imageUrl")
    private String imageUrl;
    @OneToMany(mappedBy = "featuredLocId")
    private List<Dronesession> dronesessionList;

    public FeaturedLocation() {
    }

    public FeaturedLocation(Integer id) {
        this.id = id;
    }

    public FeaturedLocation(Integer id, String title, String subTitle, String imageUrl) {
        this.id = id;
        this.title = title;
        this.subTitle = subTitle;
        this.imageUrl = imageUrl;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @XmlTransient
    public List<Dronesession> getDronesessionList() {
        return dronesessionList;
    }

    public void setDronesessionList(List<Dronesession> dronesessionList) {
        this.dronesessionList = dronesessionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FeaturedLocation)) {
            return false;
        }
        FeaturedLocation other = (FeaturedLocation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.FeaturedLocation[ id=" + id + " ]";
    }
    
}
