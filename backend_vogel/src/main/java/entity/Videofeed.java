/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mikerooijackers
 */
@Entity
@Table(name = "videofeed")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Videofeed.findAll", query = "SELECT v FROM Videofeed v"),
    @NamedQuery(name = "Videofeed.findById", query = "SELECT v FROM Videofeed v WHERE v.id = :id"),
    @NamedQuery(name = "Videofeed.findByStreamURL", query = "SELECT v FROM Videofeed v WHERE v.streamURL = :streamURL"),
    @NamedQuery(name = "Videofeed.findByFileLocation", query = "SELECT v FROM Videofeed v WHERE v.fileLocation = :fileLocation")})
public class Videofeed implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "streamURL")
    private String streamURL;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "fileLocation")
    private String fileLocation;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "videoFeedId")
    private List<Dronesession> dronesessionList;

    public Videofeed() {
    }

    public Videofeed(Integer id) {
        this.id = id;
    }

    public Videofeed(Integer id, String streamURL, String fileLocation) {
        this.id = id;
        this.streamURL = streamURL;
        this.fileLocation = fileLocation;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStreamURL() {
        return streamURL;
    }

    public void setStreamURL(String streamURL) {
        this.streamURL = streamURL;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    @XmlTransient
    public List<Dronesession> getDronesessionList() {
        return dronesessionList;
    }

    public void setDronesessionList(List<Dronesession> dronesessionList) {
        this.dronesessionList = dronesessionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Videofeed)) {
            return false;
        }
        Videofeed other = (Videofeed) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Videofeed[ id=" + id + " ]";
    }
    
}
