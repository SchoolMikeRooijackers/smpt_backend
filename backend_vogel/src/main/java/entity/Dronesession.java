/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mikerooijackers
 */
@Entity
@Table(name = "dronesession")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Dronesession.findAll", query = "SELECT d FROM Dronesession d"),
    @NamedQuery(name = "Dronesession.findById", query = "SELECT d FROM Dronesession d WHERE d.id = :id"),
    @NamedQuery(name = "Dronesession.findByStartTime", query = "SELECT d FROM Dronesession d WHERE d.startTime = :startTime"),
    @NamedQuery(name = "Dronesession.findByEndTime", query = "SELECT d FROM Dronesession d WHERE d.endTime = :endTime")})
public class Dronesession implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "startTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startTime;
    @Column(name = "endTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endTime;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dronesessionId")
    private List<Spectatorsession> spectatorsessionList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "droneSessionId")
    private List<UserDronesession> userDronesessionList;
    @JoinColumn(name = "droneId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Drone droneId;
    @JoinColumn(name = "featuredLocId", referencedColumnName = "id")
    @ManyToOne
    private FeaturedLocation featuredLocId;
    @JoinColumn(name = "pilotId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private User pilotId;
    @JoinColumn(name = "videoFeedId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Videofeed videoFeedId;

    public Dronesession() {
    }

    public Dronesession(Integer id) {
        this.id = id;
    }

    public Dronesession(Integer id, Date startTime) {
        this.id = id;
        this.startTime = startTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    @XmlTransient
    public List<Spectatorsession> getSpectatorsessionList() {
        return spectatorsessionList;
    }

    public void setSpectatorsessionList(List<Spectatorsession> spectatorsessionList) {
        this.spectatorsessionList = spectatorsessionList;
    }

    @XmlTransient
    public List<UserDronesession> getUserDronesessionList() {
        return userDronesessionList;
    }

    public void setUserDronesessionList(List<UserDronesession> userDronesessionList) {
        this.userDronesessionList = userDronesessionList;
    }

    public Drone getDroneId() {
        return droneId;
    }

    public void setDroneId(Drone droneId) {
        this.droneId = droneId;
    }

    public FeaturedLocation getFeaturedLocId() {
        return featuredLocId;
    }

    public void setFeaturedLocId(FeaturedLocation featuredLocId) {
        this.featuredLocId = featuredLocId;
    }

    public User getPilotId() {
        return pilotId;
    }

    public void setPilotId(User pilotId) {
        this.pilotId = pilotId;
    }

    public Videofeed getVideoFeedId() {
        return videoFeedId;
    }

    public void setVideoFeedId(Videofeed videoFeedId) {
        this.videoFeedId = videoFeedId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dronesession)) {
            return false;
        }
        Dronesession other = (Dronesession) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Dronesession[ id=" + id + " ]";
    }
    
}
