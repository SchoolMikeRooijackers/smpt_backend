/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mikerooijackers
 */
@Entity
@Table(name = "user_dronesession")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserDronesession.findAll", query = "SELECT u FROM UserDronesession u"),
    @NamedQuery(name = "UserDronesession.findById", query = "SELECT u FROM UserDronesession u WHERE u.id = :id")})
public class UserDronesession implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "droneSessionId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Dronesession droneSessionId;
    @JoinColumn(name = "userId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private User userId;

    public UserDronesession() {
    }

    public UserDronesession(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Dronesession getDroneSessionId() {
        return droneSessionId;
    }

    public void setDroneSessionId(Dronesession droneSessionId) {
        this.droneSessionId = droneSessionId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserDronesession)) {
            return false;
        }
        UserDronesession other = (UserDronesession) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.UserDronesession[ id=" + id + " ]";
    }
    
}
