/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mikerooijackers
 */
@Entity
@Table(name = "dronesession_location")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DronesessionLocation.findAll", query = "SELECT d FROM DronesessionLocation d"),
    @NamedQuery(name = "DronesessionLocation.findById", query = "SELECT d FROM DronesessionLocation d WHERE d.id = :id"),
    @NamedQuery(name = "DronesessionLocation.findByDroneSessionId", query = "SELECT d FROM DronesessionLocation d WHERE d.droneSessionId = :droneSessionId"),
    @NamedQuery(name = "DronesessionLocation.findByLocationId", query = "SELECT d FROM DronesessionLocation d WHERE d.locationId = :locationId"),
    @NamedQuery(name = "DronesessionLocation.findByDateTime", query = "SELECT d FROM DronesessionLocation d WHERE d.dateTime = :dateTime")})
public class DronesessionLocation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Column(name = "droneSessionId")
    private Integer droneSessionId;
    @Column(name = "locationId")
    private Integer locationId;
    @Column(name = "dateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTime;

    public DronesessionLocation() {
    }

    public DronesessionLocation(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDroneSessionId() {
        return droneSessionId;
    }

    public void setDroneSessionId(Integer droneSessionId) {
        this.droneSessionId = droneSessionId;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DronesessionLocation)) {
            return false;
        }
        DronesessionLocation other = (DronesessionLocation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DronesessionLocation[ id=" + id + " ]";
    }
    
}
