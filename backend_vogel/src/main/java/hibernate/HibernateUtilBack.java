package hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author pepijndegoede
 */
public class HibernateUtilBack {

    private static final SessionFactory sessionFactory = buildSessionDactory();

    private static SessionFactory buildSessionDactory() {
        try {
            return new Configuration().configure().buildSessionFactory();
        } catch (RuntimeException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    /**
     *
     * @return SessionFactory sessionFactory
     */
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    /**
     *
     */
    public static void shutdown() {
        getSessionFactory().close();
    }
}
