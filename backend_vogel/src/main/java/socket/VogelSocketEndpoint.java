package socket;

import de.yadrone.base.ARDrone;
import entity.Drone;
import entity.Dronesession;
import entity.Location;
import entity.User;
import entity.Videofeed;
import de.yadrone.base.IARDrone;
import de.yadrone.base.command.VideoChannel;
import de.yadrone.base.video.ImageListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import rest.ObjectResponse;
import services.SessionService;
import services.UserService;
import socket.decoders.MessageDecoder;
import socket.encoders.ActivePilotsEncoder;
import socket.encoders.ErrorMessageEncoder;
import socket.encoders.SpectatorEncoder;
import socket.messages.response.SpectatorResponseMessage;
import socket.encoders.SessionStatisticsEncoder;
import socket.messages.response.ErrorResponseMessage;
import socket.messages.RequestMessage;
import socket.messages.response.ActivePilotsResponseMessage;
import socket.messages.response.SessionStatisticsResponseMessage;
import socket.objects.DroneSocketSession;

/**
 *
 * @author Yasin
 *
 */
@ServerEndpoint(
        value = "/socket",
        decoders = {MessageDecoder.class},
        encoders = {ErrorMessageEncoder.class, ActivePilotsEncoder.class, SessionStatisticsEncoder.class, SpectatorEncoder.class}
)
public class VogelSocketEndpoint {

    private static int speed = 20; // percentage of max speed
    private int maxHeight = 1500; // max height in mm
    private int minHeight = 500; // min height in mm

    private domain.Location location = null;

    private static final Logger LOG = Logger.getLogger(VogelSocketEndpoint.class.getName());

    private static final SessionService sessionService = new SessionService();
    private static final UserService userService = new UserService();

    /**
     * Map that stores the current active pilot sessions.
     */
    private static final Map<Session, DroneSocketSession> activePilotSessions = Collections.synchronizedMap(new HashMap<Session, DroneSocketSession>());
    private long lastActionSystemMillisecond;

    /**
     * This method is called when an unexpected error occurs.
     *
     * @param client the current client that is connected
     * @param t the exception that is thrown
     */
    @OnError
    public void onError(Session client, Throwable t) {
        if (t.getStackTrace() != null && t.getStackTrace().length > 0) {
            LOG.log(Level.INFO, "Error occured: {0}", t.getStackTrace()[0].getClassName());
            LOG.log(Level.INFO, "Error occured: {0}", t.getStackTrace()[0].getMethodName());
            LOG.log(Level.INFO, "Error occured: {0}", t.getStackTrace()[0].getLineNumber());
        }
        LOG.log(Level.INFO, "Error occured: {0}", t.toString());
        LOG.log(Level.INFO, "Error occured: {0}", t.getMessage());
        sendErrorToClient(client, 301);
    }

    /**
     * This method is called when a client connects with the socket.
     *
     * @param client the current client that is connected
     * @param config the configuration for the current connected client
     */
    @OnOpen
    public void onOpen(final Session client, EndpointConfig config) {
        LOG.log(Level.INFO, "Connection opened: {0}", client.getId());

        // Add this client to the user properties to make it accessibale within the decoder class
        config.getUserProperties().put("client", client);

        // Add the client to the global list
        activePilotSessions.put(client, null);
    }

    private void mockSocketData(final Session client) {
        SessionService ss = new SessionService();
        Dronesession ds = (Dronesession) ss.getDroneSession(34).getObject();
        if (ds != null) {
            activePilotSessions.put(client, new DroneSocketSession(null, ds));
            this.mockLocationData();
        }
    }

    /**
     * This method is called when the user disconnects from the socket.
     *
     * @param client the client that has been disconnected
     * @throws java.lang.InterruptedException
     */
    @OnClose
    public void onClose(final Session client) throws InterruptedException {
        LOG.log(Level.INFO, "Connection closed: {0}", client.getId());

        if (activePilotSessions.containsKey(client)) {
            VogelSocketEndpoint.closeConnection(client);
            LOG.info("Client remove from list.");
        }
    }

    /**
     * This method is called when an user posts data to the socket.
     *
     * @param client the client that has posted the data
     * @param requestMessage the message object that the user has posted
     */
    @OnMessage
    public void onMessage(final Session client, final RequestMessage requestMessage) {
        LOG.log(Level.INFO, "Connection message received: {0}", client.getId());

        if (requestMessage == null) {
            LOG.info("Message is NULL");
            sendErrorToClient(client, 305);
            return;
        }

        if (requestMessage.getAction() == null) {
            LOG.info("Action is NULL");
            sendErrorToClient(client, 303);
            return;
        }

        if (activePilotSessions.containsKey(client)) {
            DroneSocketSession droneSocketSession = activePilotSessions.get(client);
            setLocation(droneSocketSession);
        }

        doAction(client, requestMessage);

    }

    private static void setLocation(final DroneSocketSession droneSocketSession) {
        if (droneSocketSession != null) {
            Location currentLocation = new Location();
            currentLocation.setLatitude(1F);
            currentLocation.setLongitude(1F);
            currentLocation.setAltitude(1F);

            LOG.info("Updated Current Location");
        }
    }

    private void doAction(final Session client, final RequestMessage requestMessage) {
        switch (requestMessage.getAction()) {
            case ACTIVE_PILOTS:
                sendMessageToClient(client, mockDataResponse());
                break;
            case OPEN_SPECTATOR_SESSION:
                this.openSpectatorSession(client, requestMessage);
                break;
            case SESSION_STATISTICS:
                sendMessageToClient(client, getSessionStatistics(requestMessage));
                break;
            case START_DRONE:
                this.startDrone(client, requestMessage);
                break;
            case TAKE_OFF:
                VogelSocketEndpoint.takeOff(client);
                break;
            case EMERGENCY:
                VogelSocketEndpoint.emergency(client);
                break;
            case OPEN_CONNECTION:
                this.openConnection(client);
                break;
            case CLOSE_CONNECTION:
                VogelSocketEndpoint.closeConnection(client);
                break;
            case HOVER:
                this.hover(client);
                break;
            case LANDING:
                this.landing(client);
                break;
            case LEFT:
                VogelSocketEndpoint.left(client);
                break;
            case RIGHT:
                this.right(client);
                break;
            case FORWARD:
                this.foward(client);
                break;
            case BACKWARD:
                this.backward(client);
                break;
            case DOWN:
                this.down(client);
                break;
            case UP:
                this.up(client);
                break;
            case SPIN_LEFT:
                this.spinLeft(client);
                break;
            case SPIN_RIGHT:
                this.spinRight(client);
                break;
            default:
                sendErrorToClient(client, 304);
                break;
        }
    }

    /**
     * This method is used to communicate with the client.
     *
     * @param client the receiver/destination of the message
     * @param message the object that has to be send to the client
     */
    public void sendMessageToClient(Session client, Object message) {
        if (client.isOpen()) {
            client.getAsyncRemote().sendObject(message);
            LOG.info("Response sent");
        }
    }

    /**
     * This method is used to send error messages to the client.
     *
     * @param client the receiver/destination of the error
     * @param errorCode the type of error that occurred
     */
    public static void sendErrorToClient(Session client, Integer errorCode) {
        List<Integer> errorCodes = new ArrayList<>();
        errorCodes.add(errorCode);

        if (client.isOpen()) {
            client.getAsyncRemote().sendObject(new ErrorResponseMessage(errorCodes));
            LOG.info("Error sent");
        }
    }

    /**
     * Start the drone
     *
     * @param requestMessage
     */
    private void startDrone(Session client, RequestMessage requestMessage) {
        int userId = requestMessage.getUserId();
        //int droneSessionId = requestMessage.getDroneSessionId();

        User user = null;
        Dronesession dronesession;

        // Get the user
        ObjectResponse userResponse = userService.getUser(userId);
        if (!userResponse.succes()) {
            sendErrorToClient(client, 308);
            return;
        }

        // Get the dronesession
        //dronesession = getDroneSession(user, droneSessionId);
        /*if (dronesession == null) {
            sendErrorToClient(client, 306);
            return;
        }*/
        // Does the user already have a DroneSession
        DroneSocketSession droneSocketSession = activePilotSessions.get(client);
        if (droneSocketSession != null) {
            sendErrorToClient(client, 307);
            return;
        }

        IARDrone drone = new ARDrone();
        activePilotSessions.put(client, new DroneSocketSession(drone, null)); // new ARDrone()
        //this.mockLocationData();
        this.openConnection(client);

        /*Timer timerLastAction = new Timer();

        timerLastAction.schedule(new TimerTask() {
            /**
             * Check for tje last action
             */
       /*     @Override
            public void run() {

                checkLastAction(System.currentTimeMillis() % 1000, client);
            }
        }, 500);*/
    }

    private void checkLastAction(long currentTimeInMilliseconds, Session client) {
        long diffrence = currentTimeInMilliseconds - this.lastActionSystemMillisecond;

        if (diffrence > 1000) {
            this.hover(client);
        }

        if (diffrence > 20000) {
            this.landing(client);
        }
    }

    /**
     * Start a spectator session
     *
     * @param client
     * @param rm
     * @return Dronesession
     */
    public SpectatorResponseMessage openSpectatorSession(Session client, RequestMessage rm) {
        Collection<DroneSocketSession> active;
        List<Integer> ids = new ArrayList<>();

        if (client.isOpen()) {
            active = activePilotSessions.values();
            active.stream().forEach((s) -> {
                if (s != null) {
                    ids.add(s.getDroneSessionId());
                }
            });

            if (ids.contains(rm.getDroneSessionId())) {
                ObjectResponse response = sessionService.openSpectatorSession(rm.getUserId(), rm.getDroneSessionId());
                if (response.succes()) {
                    Dronesession dronesession = (Dronesession) response.getObject();
                    return new SpectatorResponseMessage(dronesession);
                }
            } else {
                sendErrorToClient(client, 310); // 310 - drone not found
            }
        }
        return null;
    }

    private static Dronesession getDroneSession(User user, int droneSessionId) {
        ObjectResponse response = sessionService.getDroneSession(droneSessionId);
        if (response.succes()) {
            Location currLoc1 = new Location();
            currLoc1.setLatitude(1F);
            currLoc1.setLongitude(1F);
            currLoc1.setAltitude(1F);

            Location baseLoc1 = new Location();
            baseLoc1.setLatitude(1F);
            baseLoc1.setLongitude(1F);
            baseLoc1.setAltitude(1F);

            Drone d1 = new Drone();
            d1.setBaseLocationId(baseLoc1);
            d1.setCurrentLocationId(currLoc1);

            Dronesession ds1 = (Dronesession) response.getObject();
            ds1.setStartTime(new Date());
            ds1.setEndTime(null);
            ds1.setDroneId(d1);
            ds1.setPilotId(user);

            Videofeed vf1 = new Videofeed(1, "streamurl.com", "filelocation.com");
            ds1.setPilotId(user);
            ds1.setVideoFeedId(vf1);

            return ds1;
        }
        return null;
    }

    private static void closeDroneSession(Session client, int droneSessionId) {
        ObjectResponse response = sessionService.closeDroneSession(droneSessionId);
        if (!response.succes()) {
            sendErrorToClient(client, 306);
        }
    }

    private static IARDrone getDrone(Session client) {
        DroneSocketSession droneSocketSession = activePilotSessions.get(client);
        if (droneSocketSession == null) {
            sendErrorToClient(client, 309);
            return null;
        }

        if (droneSocketSession.getDrone() == null) {
            return null;
        }

        return droneSocketSession.getDrone();
    }

    /**
     * The mock data will be temporary used to return data from the socket.
     *
     * @return an ActivePilotsResponseMessage object that contains a list of
     * Current DroneSession objects
     */
    private ActivePilotsResponseMessage mockDataResponse() {
        List<Dronesession> droneSessions = new ArrayList<>();

        activePilotSessions.keySet().stream().map((s) -> activePilotSessions.get(s)).filter((dss) -> (dss != null)).forEach((dss) -> {
            droneSessions.add(dss.getDroneSession());
        });

        return new ActivePilotsResponseMessage(droneSessions);
    }

    /**
     * Create every one second a new domain.Location-object containing mockdata
     */
    private void mockLocationData() {
        Timer timerLocation = new Timer();
        Random rand = new Random();
        timerLocation.schedule(new MockLocation(rand), 0, 1000);
    }

    /**
     * Create a mocklocation
     */
    public class MockLocation extends TimerTask {

        private Random rand;
        private static final float MINLATITUDE = 51.4277500f;
        private static final float MINLONGIDTUDE = 005.4563333f;
        private static final float MAXLATITUDE = 51.4524444f;
        private static final float MAXLONGITUDE = 005.4970556f;

        /**
         * Constructor for standard mocking location
         *
         * @param rand
         */
        public MockLocation(Random rand) {
            this.rand = rand;
        }

        @Override
        public void run() {

            VogelSocketEndpoint.this.location = new domain.Location(
                    0,
                    this.rand.nextFloat() * (MockLocation.MAXLATITUDE - MockLocation.MINLATITUDE) + MockLocation.MINLATITUDE,
                    this.rand.nextFloat() * (MockLocation.MAXLONGITUDE - MockLocation.MINLONGIDTUDE) + MockLocation.MINLONGIDTUDE,
                    this.rand.nextFloat() * (VogelSocketEndpoint.this.maxHeight - VogelSocketEndpoint.this.minHeight) + VogelSocketEndpoint.this.minHeight
            );
        }
    }

    private static void takeOff(Session client) {
        LOG.info("TAKE OFF");
        IARDrone drone = getDrone(client);
        if (drone != null) {
            try {
                drone.takeOff();
            } catch (Exception e) {
                LOG.info(e.toString());
            } finally {
                if (drone != null) {
                    drone.hover();
                }
            }
        }
    }

    private static void emergency(Session client) {
        LOG.info("EMERGENCY");
        IARDrone drone = getDrone(client);
        if (drone != null) {
            try {
                drone.getCommandManager().emergency();
            } catch (Exception e) {
                LOG.info(e.toString());
                drone.landing();
            } finally {
                drone.landing();
            }
        }
    }

    private void openConnection(Session client) {
        LOG.info("CONNECT");
        IARDrone drone = getDrone(client);
        if (drone != null) {
            try {
                drone.start();
                drone.setMaxAltitude(maxHeight);
                drone.setMinAltitude(minHeight);
                drone.setSpeed(speed);
                
                drone.getCommandManager().setVideoChannel(VideoChannel.HORI);

                drone.getVideoManager().addImageListener(new ImageListener() {

                    /*
                        If an image has been updated.
                     */
                    @Override
                    public void imageUpdated(BufferedImage newImage) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                //videoListenerInterface.sendImageFrame(newImage);
                            }
                        }).start();
                    }
                });
            } catch (Exception e) {
                LOG.info(e.toString());
                drone.landing();
            }
        }
    }

    private static void closeConnection(Session client) {
        LOG.info("CLOSE");
        DroneSocketSession dss = activePilotSessions.get(client);
        if (dss == null) {
            sendErrorToClient(client, 309);
            return;
        }

        IARDrone drone = getDrone(client);
        if (drone != null) {
            try {
                drone.landing();
                closeDroneSession(client, dss.getDroneSession().getId());
            } catch (Exception e) {
                LOG.info(e.toString());
                drone.getCommandManager().emergency();
            }
        }
    }

    private void hover(Session client) {
        LOG.info("HOVER");
        IARDrone drone = getDrone(client);
        if (drone != null) {
            try {
                drone.hover();
            } catch (Exception e) {
                LOG.info(e.toString());
            } finally {
                //this.lastAction();
            }
        }

    }

    private void landing(Session client) {
        LOG.info("LANDING");
        IARDrone drone = getDrone(client);
        if (drone != null) {
            try {
                drone.landing();
                drone.stop();
            } catch (Exception e) {
                LOG.info(e.toString());
            } finally {
                //this.lastAction();
            }
        }

    }

    private static void left(Session client) {
        LOG.info("LEFT");
        IARDrone drone = getDrone(client);
        if (drone != null) {
            try {
                drone.goLeft();
            } catch (Exception e) {
                LOG.info(e.toString());
                drone.hover();
            }
        }
    }

    private void right(Session client) {
        LOG.info("RIGHT");
        IARDrone drone = getDrone(client);
        if (drone != null) {
            try {
                drone.goRight();
            } catch (Exception e) {
                LOG.info(e.toString());
                drone.hover();
            } finally {
                //this.lastAction();
            }
        }
    }

    private void foward(Session client) {
        LOG.info("FORWARD");
        IARDrone drone = getDrone(client);
        if (drone != null) {
            try {
                drone.forward();
            } catch (Exception e) {
                LOG.info(e.toString());
                drone.hover();
            } finally {
                //this.lastAction();
            }
        }

    }

    private void backward(Session client) {
        LOG.info("BACKWARD");
        IARDrone drone = getDrone(client);
        if (drone != null) {
            try {
                drone.backward();
            } catch (Exception e) {
                LOG.info(e.toString());
                drone.hover();
            } finally {
               // this.lastAction();
            }
        }

    }

    private void down(Session client) {
        LOG.info("DOWN");
        IARDrone drone = getDrone(client);
        if (drone != null) {
            try {
                drone.down();
            } catch (Exception e) {
                LOG.info(e.toString());
                drone.hover();
            } finally {
               // this.lastAction();
            }
        }

    }

    private void up(Session client) {
        LOG.info("UP");
        IARDrone drone = getDrone(client);
        if (drone != null) {
            try {
                drone.up();
            } catch (Exception e) {
                LOG.info(e.toString());
                drone.hover();
            } finally {
               // this.lastAction();
            }
        }

    }

    private void spinLeft(Session client) {
        LOG.info("SPIN LEFT");
        IARDrone drone = getDrone(client);
        if (drone != null) {
            try {
                drone.spinLeft();
            } catch (Exception e) {
                LOG.info(e.toString());
                drone.hover();
            } finally {
               // this.lastAction();
            }
        }

    }

    private void spinRight(Session client) {
        LOG.info("SPIN RIGHT");
        IARDrone drone = getDrone(client);
        if (drone != null) {
            try {
                drone.spinRight();
            } catch (Exception e) {
                LOG.info(e.toString());
                drone.hover();
            } finally {
               // this.lastAction();
            }
        }

    }

    private void lastAction() {
        this.lastActionSystemMillisecond = System.currentTimeMillis() % 1000;
    }

    /**
     * Returns the statistics of a session including mock locations.
     *
     * @param requestMessage
     * @return SessionStatisticsResponeMessage-object
     */
    private SessionStatisticsResponseMessage getSessionStatistics(RequestMessage requestMessage) {
        int dronesessionId = requestMessage.getDroneSessionId();
        SessionStatisticsResponseMessage sessionStatisticsResponeMessage = new SessionStatisticsResponseMessage();

        for (Map.Entry<Session, DroneSocketSession> entry : activePilotSessions.entrySet()) {
            Session s = entry.getKey();

            DroneSocketSession droneSocketSession = activePilotSessions.get(s);
            if (droneSocketSession != null) {
                Dronesession droneSession = droneSocketSession.getDroneSession();
                if (droneSession != null && droneSession.getId() == dronesessionId) {
                    sessionStatisticsResponeMessage.setPilotName("Henk");
                    sessionStatisticsResponeMessage.setCurrentLocation(this.location);
                    sessionStatisticsResponeMessage.setSpeed(this.speed);
                    return sessionStatisticsResponeMessage;
                }
            }
        }

        return null;
    }
}
