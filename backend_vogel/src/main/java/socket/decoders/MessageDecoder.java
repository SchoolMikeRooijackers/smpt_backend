package socket.decoders;

import com.google.gson.Gson;
import socket.messages.RequestMessage;
import socket.objects.ActionEnum;
import javax.json.Json;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;
import java.io.StringReader;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.Session;
import socket.VogelSocketEndpoint;
import socket.messages.request.ActivePilotsRequestMessage;

/**
 * Created by Yasin This class is used to decode the incoming String message and
 * to parse it to an Object
 */
public class MessageDecoder implements Decoder.Text<RequestMessage> {

    // LOG
    private static final Logger LOG = Logger.getLogger(MessageDecoder.class.getName());

    // USER PROPERTIES
    private Map<String, Object> userProperties;
    private static final String CLIENT = "client";

    // ACTION
    private static final String ACTION_NAME = "action";

    /**
     * This method decodes the posted JSON string by the client to an object.
     *
     * @param string the JSON string that is posted by the client
     * @return A message object that has been created by parsing JSON string to
     * object
     * @throws DecodeException when the decoding fails
     */
    @Override
    public RequestMessage decode(String string) throws DecodeException {
        // The JSONObject that has been received
        JsonObject json = Json.createReader(new StringReader(string)).readObject();

        LOG.log(Level.INFO, "Received Message: {0}", string);

        // Check if the JSONObject contains an ACTION from the ActionEnum
        if (!json.containsKey(ACTION_NAME)) {
            sendErrorResponse(303);
            return null;
        }

        // Get the Action
        String action = json.getString(ACTION_NAME);
        ActionEnum actionEnum;

        try {
            actionEnum = ActionEnum.valueOf(ActionEnum.class, action.toUpperCase());
        } catch (IllegalArgumentException iae) {
            sendErrorResponse(304);
            LOG.info(iae.getMessage());
            return null;
        }

        // Parse and Return the Object
        return parseJsonToObject(actionEnum, json);
    }

    /**
     * This method checks if the JSON string is a valid JSON.
     *
     * @param string the JSON String
     * @return a boolean, true if the JSON is valid and false if not
     */
    @Override
    public boolean willDecode(String string) {
        try {
            LOG.info(string);
            Json.createReader(new StringReader(string)).read();
            return true;
        } catch (JsonException ex) {
            LOG.info(ex.getMessage());
            sendErrorResponse(302);
            return false;
        }
    }

    /**
     * The configuration for the connected client, contains the client object as
     * well to make it possible to communicate with the client within the
     * decoder class.
     *
     * @param config the map that contains config settings for the client
     */
    @Override
    public void init(EndpointConfig config) {
        userProperties = config.getUserProperties();
    }

    /**
     * Called upon destroy. Not used.
     */
    @Override
    public void destroy() {
    }

    /**
     * Parse the JSONObject to the correct Object.
     *
     * @param actionEnum the action that the user wants to perform
     * @param json the JSONObject created by the JSON string that the user has
     * posted
     * @return
     */
    private RequestMessage parseJsonToObject(ActionEnum actionEnum, JsonObject json) {
        switch (actionEnum) {
            case OPEN_SPECTATOR_SESSION:
                LOG.info("Action: OPEN_SPECTATOR_SESSION");
                return new Gson().fromJson(json.toString(), RequestMessage.class);
            case ACTIVE_PILOTS:
                LOG.info("Action: ACTIVE_PILOTS");
                return new Gson().fromJson(json.toString(), ActivePilotsRequestMessage.class);
            case SESSION_STATISTICS:
                LOG.info("Action: SESSION_STATISTICS");
                return new Gson().fromJson(json.toString(), RequestMessage.class);
            case OPEN_CONNECTION:
                LOG.info("Commando: OPEN_CONNECTION");
                return new Gson().fromJson(json.toString(), RequestMessage.class);
            case CLOSE_CONNECTION:
                LOG.info("Commando: CLOSE_CONNECTION");
                return new Gson().fromJson(json.toString(), RequestMessage.class);
            case START_DRONE:
                LOG.info("Commando: START_DRONE");
                return new Gson().fromJson(json.toString(), RequestMessage.class);
            case TAKE_OFF:
                LOG.info("Action: TAKE_OFF");
                return new Gson().fromJson(json.toString(), RequestMessage.class);
            case EMERGENCY:
                LOG.info("Commando: EMERGENCY");
                return new Gson().fromJson(json.toString(), RequestMessage.class);
            case HOVER:
                LOG.info("Commando: HOVER");
                return new Gson().fromJson(json.toString(), RequestMessage.class);
            case LANDING:
                LOG.info("Commando: LANDING");
                return new Gson().fromJson(json.toString(), RequestMessage.class);
            case LEFT:
                LOG.info("Commando: LEFT");
                return new Gson().fromJson(json.toString(), RequestMessage.class);
            case RIGHT:
                LOG.info("Commando: RIGHT");
                return new Gson().fromJson(json.toString(), RequestMessage.class);
            case FORWARD:
                LOG.info("Commando: FORWARD");
                return new Gson().fromJson(json.toString(), RequestMessage.class);
            case BACKWARD:
                LOG.info("Commando: BACKWARD");
                return new Gson().fromJson(json.toString(), RequestMessage.class);
            case DOWN:
                LOG.info("Commando: DOWN");
                return new Gson().fromJson(json.toString(), RequestMessage.class);
            case UP:
                LOG.info("Commando: UP");
                return new Gson().fromJson(json.toString(), RequestMessage.class);
            case SPIN_LEFT:
                LOG.info("Commando: SPIN_LEFT");
                return new Gson().fromJson(json.toString(), RequestMessage.class);
            case SPIN_RIGHT:
                LOG.info("Commando: SPIN_RIGHT");
                return new Gson().fromJson(json.toString(), RequestMessage.class);
            default:
                sendErrorResponse(304);
                return null;
        }
    }

    /**
     * This method sends error that occurs while decoding in the decoder class
     * to the client.
     *
     * @param errorCode
     */
    private void sendErrorResponse(Integer errorCode) {
        // Check if the userproperties are found
        if (userProperties != null && userProperties.containsKey(CLIENT)) {
            VogelSocketEndpoint.sendErrorToClient((Session) userProperties.get(CLIENT), errorCode);
        }
    }

}
