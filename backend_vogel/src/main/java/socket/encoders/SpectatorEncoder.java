/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socket.encoders;

import com.google.gson.Gson;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;
import socket.messages.response.SpectatorResponseMessage;

/**
 *
 * @author thiennguyen
 */
public class SpectatorEncoder implements Encoder.Text<SpectatorResponseMessage> {

    @Override
    public String encode(SpectatorResponseMessage object) throws EncodeException {
        return new Gson().toJson(object);
    }

    @Override
    public void init(EndpointConfig config) {
    }

    @Override
    public void destroy() {
    }
    
}
