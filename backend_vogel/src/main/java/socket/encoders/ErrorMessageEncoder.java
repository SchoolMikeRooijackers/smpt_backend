package socket.encoders;

import com.google.gson.Gson;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;
import socket.messages.response.ErrorResponseMessage;

/**
 *
 * @author Yasin
 */
public class ErrorMessageEncoder implements Encoder.Text<ErrorResponseMessage> {

    /**
     *
     * @param object
     * @return
     * @throws EncodeException
     */
    @Override
    public String encode(ErrorResponseMessage object) throws EncodeException {
        return new Gson().toJson(object);
    }

    /**
     *
     * @param config
     */
    @Override
    public void init(EndpointConfig config) {
    }

    /**
     *
     */
    @Override
    public void destroy() {
    }

}
