package socket.encoders;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.logging.Logger;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;
import socket.messages.response.ActivePilotsResponseMessage;

/**
 *
 * @author Yasin
 */
public class ActivePilotsEncoder implements Encoder.Text<ActivePilotsResponseMessage> {

    // LOG
    private static final Logger LOG = Logger.getLogger(ActivePilotsEncoder.class.getName());
    
    /**
     *
     * @param object
     * @return
     * @throws EncodeException
     * Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();
     */
    @Override
    public String encode(ActivePilotsResponseMessage object) throws EncodeException {
        try {
            // Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();
            // String json = new Gson().toJson(object);
            // System.out.println("JSON: " + json);
            
            final GsonBuilder builder = new GsonBuilder();
            builder.excludeFieldsWithoutExposeAnnotation();
            final Gson gson = builder.create();
            
            String json = gson.toJson(object);
            
            return json;
        } catch (Exception e) {
            LOG.info(e.getMessage());
        }
        return "";
    }

    /**
     *
     * @param config
     */
    @Override
    public void init(EndpointConfig config) {
    }

    /**
     *
     */
    @Override
    public void destroy() {
    }

}
