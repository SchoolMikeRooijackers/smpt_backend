package socket.encoders;

import com.google.gson.Gson;
import java.util.logging.Logger;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;
import socket.messages.response.SessionStatisticsResponseMessage;

/**
 *
 * @author Yasin
 */
public class SessionStatisticsEncoder implements Encoder.Text<SessionStatisticsResponseMessage> {

    private static final Logger LOG = Logger.getLogger(SessionStatisticsEncoder.class.getName());
    
    /**
     *
     * @param object
     * @return
     * @throws EncodeException
     */
    @Override
    public String encode(SessionStatisticsResponseMessage object) throws EncodeException {
        try {
            String json = new Gson().toJson(object);
            return json;
        } catch (Exception e) {
            LOG.info(e.getMessage());
        }
        return "";
    }

    /**
     *
     * @param config
     */
    @Override
    public void init(EndpointConfig config) {
    }

    /**
     *
     */
    @Override
    public void destroy() {
    }
}
