package socket.messages.response;

import com.google.gson.annotations.Expose;
import entity.Dronesession;
import java.util.ArrayList;
import java.util.List;
import socket.messages.ResponseMessage;

/**
 *
 * @author Yasin
 */
public class ActivePilotsResponseMessage {

    @Expose(serialize = true)
    private List<Dronesession> activePilots = new ArrayList<>();
    
    public ActivePilotsResponseMessage() {
        
    }

    /**
     * Default constructor.
     * @param activePilots
     */
    public ActivePilotsResponseMessage(List<Dronesession> activePilots) {
        this.activePilots = activePilots;
    }

    /**
     * Return a list of active pilots.
     * @return List activePilots
     */
    public List<Dronesession> getActivePilots() {
        return this.activePilots;
    }

}
