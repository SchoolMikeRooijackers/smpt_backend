/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socket.messages.response;

import entity.Dronesession;
import socket.messages.ResponseMessage;

/**
 *
 * @author thiennguyen
 */
public class SpectatorResponseMessage extends ResponseMessage {

    private final Dronesession dronesession;

    /**
     * Default constructor
     * @param dronesession 
     */
    public SpectatorResponseMessage(Dronesession dronesession) {
        this.dronesession = dronesession;
    }

    /**
     * Default getter
     * @return 
     */
    public Dronesession getDronesession() {
        return dronesession;
    }

}
