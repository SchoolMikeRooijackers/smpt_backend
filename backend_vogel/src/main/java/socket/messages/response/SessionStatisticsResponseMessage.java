/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socket.messages.response;

import com.google.gson.annotations.Expose;
import domain.Location;

/**
 *
 * @author hrutgers
 */
public class SessionStatisticsResponseMessage {
    @Expose(serialize = true)
    private String pilotName;
    @Expose(serialize = true)
    private Location currentLocation;
    @Expose(serialize = true)
    private float speed;
    
    /**
     *
     */
    public SessionStatisticsResponseMessage() {
    }

    /**
     *
     * @return
     */
    public String getPilotName() {
        return pilotName;
    }

    /**
     *
     * @param pilotName
     */
    public void setPilotName(String pilotName) {
        this.pilotName = pilotName;
    }

    /**
     *
     * @return
     */
    public Location getCurrentLocation() {
        return currentLocation;
    }

    /**
     *
     * @param currentLocation
     */
    public void setCurrentLocation(Location currentLocation) {
        this.currentLocation = currentLocation;
    }

    /**
     *
     * @return
     */
    public float getSpeed() {
        return speed;
    }

    /**
     *
     * @param speed
     */
    public void setSpeed(float speed) {
        this.speed = speed;
    }
}
