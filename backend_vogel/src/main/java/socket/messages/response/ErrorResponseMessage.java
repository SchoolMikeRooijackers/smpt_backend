package socket.messages.response;

import java.util.List;
import socket.messages.ResponseMessage;

/**
 *
 * @author Yasin
 */
public class ErrorResponseMessage extends ResponseMessage {

    private final List<Integer> errors;

    /**
     *
     * @param errors
     */
    public ErrorResponseMessage(List<Integer> errors) {
        this.errors = errors;
    }
    
    /**
     *
     * @param errorCode
     * @return
     */
    public boolean addError(Integer errorCode) {
        return this.errors.add(errorCode);
    }

}
