package socket.messages.request;

import socket.messages.RequestMessage;
import socket.objects.ActionEnum;

/**
 *
 * @author Yasin
 */
public class ActivePilotsRequestMessage extends RequestMessage {

    /**
     *
     * @param actionEnum
     * @param speed
     */
    public ActivePilotsRequestMessage(ActionEnum actionEnum, int speed) {
        super(actionEnum, speed);
    }

}
