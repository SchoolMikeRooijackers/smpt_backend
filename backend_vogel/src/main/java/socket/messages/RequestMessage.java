package socket.messages;

import com.google.gson.annotations.SerializedName;
import socket.objects.ActionEnum;

/**
 *
 * @author Yasin
 */
public class RequestMessage {

    /**
     * The action that the user wants to perform.
     * This property is required for the Decocer class to determine which action is called by the user.
     */
    @SerializedName("action")
    private ActionEnum action;

    /**
     * The speed of the drone.
     */
    private int speed;
    
    private int userId;
    
    private int droneSessionId;
    
    /**
     *
     * @param action
     */
    public RequestMessage(ActionEnum action) {
        this.action = action;
    }
    
    /**
     *
     * @param actionEnum
     * @param speed
     */
    public RequestMessage(ActionEnum actionEnum, int speed) {
        this.action = actionEnum;
        this.speed = speed;
    }
    
    /**
     *
     * @param actionEnum
     * @param userId
     * @param droneSessionId
     */
    public RequestMessage(ActionEnum actionEnum, int userId, int droneSessionId) {
        this.action = actionEnum;
        this.userId = userId;
        this.droneSessionId = droneSessionId;
    }

    /**
     * Return the current action that the user wants to perform.
     * @return an Action Enum
     */
    public ActionEnum getAction() {
        return this.action;
    }

    /**
     * Return the speed.
     * @return speed in integer
     */
    public int getSpeed() {
        return speed;
    }

    /**
     *
     * @return
     */
    public int getUserId() {
        return userId;
    }

    /**
     *
     * @return
     */
    public int getDroneSessionId() {
        return droneSessionId;
    }

}
