package socket.objects;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author Yasin
 */
public enum ActionEnum {
    /**
     * COMMON ACTIONS
     */
    @SerializedName("active_pilots")
    ACTIVE_PILOTS,
    @SerializedName("open_spectator_session")
    OPEN_SPECTATOR_SESSION,
    @SerializedName("session_statistics")
    SESSION_STATISTICS,
    
    /**
     * DRONE COMMANDS ACTIONS
     */
    @SerializedName("open_connection")
    OPEN_CONNECTION,
    @SerializedName("close_connection")
    CLOSE_CONNECTION, // ?
    @SerializedName("emergency")
    EMERGENCY, // [speed]
    
    @SerializedName("take_off")
    TAKE_OFF, // opstijgen
    @SerializedName("hover")
    HOVER, // zweven
    @SerializedName("landing")
    LANDING, // landen

    @SerializedName("left")
    LEFT, // [speed]
    @SerializedName("right")
    RIGHT, // [speed]
    @SerializedName("forward")
    FORWARD, // [speed]
    @SerializedName("backward")
    BACKWARD, // [speed]
    @SerializedName("down")
    DOWN, // [speed]
    @SerializedName("up")
    UP, // [speed]
    @SerializedName("spin_left")
    SPIN_LEFT, // roteren [speed]

    @SerializedName("spin_right")
    SPIN_RIGHT, // roteren [speed]
    
    @SerializedName("start_drone")
    START_DRONE;
    
    @Override    
    public String toString() {
        return name().toLowerCase();
    }
}
