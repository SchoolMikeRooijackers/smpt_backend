package socket.objects;

import de.yadrone.base.IARDrone;
import entity.Dronesession;

/**
 *
 * @author Yasin
 */
public class DroneSocketSession {
    
    private IARDrone drone;
    private Dronesession droneSession;
    
    /**
     *
     * @param drone
     * @param dronesession
     */
    public DroneSocketSession(IARDrone drone, Dronesession dronesession) {
        this.drone = drone;
        this.droneSession = dronesession;
    }
    
    /**
     *
     * @return
     */
    public IARDrone getDrone() {
        return drone;
    }
    
    /**
     *
     * @return
     */
    public Dronesession getDroneSession() {
        return droneSession;
    }
    
    /**
     * 
     * @return 
     */
    public int getDroneSessionId() {
        return droneSession.getId();
    }
    
}
