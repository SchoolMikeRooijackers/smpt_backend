package services;

import entity.User;
import hibernate.HibernateUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Login;
import models.Register;
import org.hibernate.Criteria;
import rest.ObjectResponse;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author pepijndegoede
 */
public class UserService {
    private static final String REGEX_CHARS = "[a-zA-Z --'\\\\u00C0-\\\\u00ff]";
    
    private static final String REGEX_USERNAME = "[a-zA-Z0-9]{5,20}";
    private static final String REGEX_PASSWORD = "[a-z0-9]{40}";
    
    private static final String REGEX_CITY_COUNTRY_NAME = REGEX_CHARS + "{1,40}";
    private static final String REGEX_SUFFIX = REGEX_CHARS + "{0,5}";
    private static final String REGEX_STREET = REGEX_CHARS + "{1,20}";
    private static final String REGEX_PHONE = REGEX_CHARS + "{10,20}";
    private static final String REGEX_GENDER = "[fm]{1}";
    private static final String REGEX_EMAIL = "[A-Z0-9a-z._%+-]+@[A-Z0-9a-z._%+-]+\\.[a-z]+";
    private static final String REGEX_ZIPCODE = "[1-9][0-9]{3}[\\s]?[A-Za-z]{2}";
    
    /**
     *
     * @return session for hibernate
     */
    private Session getSession() {
        try {
            return HibernateUtil.getSessionFactory().getCurrentSession();
        } catch (org.hibernate.HibernateException he) {
            Logger.getGlobal().log(Level.INFO, "HibernateException : {0}", he.getMessage());
            return HibernateUtil.getSessionFactory().openSession();
        }
    }
    
    /**
     * 
     * @param login
     * @return
     */
    public ObjectResponse loginUser(Login login) {
        ObjectResponse response = new ObjectResponse();

        response = validateValue(login.username, REGEX_USERNAME, 103, 102, response);
        response = validateValue(login.password, REGEX_PASSWORD, 114, 111, response);

        if (response.noErrors()) {
            Session session = getSession();
            try {

                //Check in database
                Criteria c = session.createCriteria(User.class);
                c.add(Restrictions.eq("username", login.username));
                c.add(Restrictions.eq("password", login.password));
                User u = (User) c.uniqueResult();

                if (u != null) {
                    response.addError(0);
                    response.setObject(u);
                } else {
                    response.addError(127); //username and password do not match 
                    return response;
                }
                return response;
            } catch (Exception e) {
                throw e;
            } finally {
                session.close();
            }
        }
        return response;
    }

    /**
     * 
     * @param id
     * @return
     */
    public ObjectResponse getUser(int id) {
        Session s = getSession();
        ObjectResponse response = new ObjectResponse();
        try {
            User u = (User) s.get(User.class, id);

            if (u != null) {
                response.setObject(u);
                response.addError(0);
            } else {
                response.addError(101);
            }
            return response;
        } catch (Exception e) {
            throw e;
        } finally {
            s.close();
        }
    }

    /**
     *
     * @param register object that is being posted
     * @return response with error codes and if success the id of the created
     * user
     */
    public ObjectResponse registerUser(models.Register register) {

        ObjectResponse response = validateValues(register);

        //If there is no error, saving the data in the database
        if (response.noErrors()) {

            Session session = getSession();

            try {
                Criteria c = session.createCriteria(entity.User.class);
                c.add(Restrictions.eq("email", register.getEmail()));
                c.setProjection(Projections.rowCount());

                Integer count = ((Number) c.setProjection(Projections.rowCount()).uniqueResult()).intValue();

                if (count > 0) {
                    response.addError(125);
                }
            } catch (Exception e) {
                Logger.getGlobal().info(e.getMessage());
            } finally {
                session.close();
            }

            if (!response.noErrors()) {
                return response;
            }

            entity.User user = new entity.User();
            entity.Address address = new entity.Address();

            session = getSession();

            try {
                session.beginTransaction();

                address.setCity(register.getCity());
                address.setCountry(register.getCountry());
                address.setHouseNumber(register.getHouseNumber());
                address.setStreet(register.getStreet());
                address.setZipcode(register.getZipcode());
                address.setSuffix(register.getSuffix());

                session.save(address);
                session.getTransaction().commit();
            } catch (Exception e) {
                Logger.getGlobal().info(e.getMessage());
            } finally {
                session.close();
            }

            session = getSession();

            try {
                session.beginTransaction();

                user.setName(register.getName());
                user.setGender(register.getGender().charAt(0));
                user.setUsername(register.getUsername());
                user.setPassword(register.getPassword());
                user.setPhone(register.getPhone());
                user.setEmail(register.getEmail());
                user.setAddressId(address);
                user.setAuthorizationType(0);
                user.setUserType(0);

                session.save(user);
                session.getTransaction().commit();

                response.object = user;
            } catch (Exception e) {
                Logger.getGlobal().info(e.getMessage());
            } finally {
                session.close();
            }
            response.addError(0);
        }     
        return response;
    }

    /**
     *
     * @param userId
     * @return
     */
    public ObjectResponse deleteUser(int userId) {

        ObjectResponse response = new ObjectResponse();
        Session session = getSession();
        try {
            session.getTransaction().begin();
            entity.User user = (entity.User) session.get(entity.User.class, userId);
            entity.Address address = (entity.Address) session.get(entity.Address.class, user.getAddressId().getId());
            session.delete(address);
            session.delete(user);
            session.getTransaction().commit();
            
        } catch (Exception e) {
            Logger.getGlobal().info(e.getMessage());
        } finally {
            session.close();
        }
        response.addError(126);
        return response;
    }

    private boolean notNullOrEmty(String string) {
        return string != null && !string.isEmpty();
    }

    private ObjectResponse validateValue(String value, String regex, int errorCodeRegex, int errorCodeIfNull, ObjectResponse response) {
        if (notNullOrEmty(value)) {
            if (!value.matches(regex)) {
                response.addError(errorCodeRegex);
            }
        } else {
            response.addError(errorCodeIfNull);
        }
        return response;
    }

    private ObjectResponse validateValues(Register register) {
        ObjectResponse response = new ObjectResponse();

        //Validate all properties
        response = validateValue(register.getCity(), REGEX_CITY_COUNTRY_NAME, 105, 104, response);
        response = validateValue(register.getCountry(), REGEX_CITY_COUNTRY_NAME, 107, 106, response);
        response = validateValue(register.getStreet(), REGEX_STREET, 111, 112, response);
        response = validateValue(register.getGender(), REGEX_GENDER, 115, 116, response);     
        response = validateValue(register.getName(), REGEX_CITY_COUNTRY_NAME, 117, 118, response);       
        response = validateValue(register.getEmail(), REGEX_EMAIL, 119, 120, response);       
        response = validateValue(register.getZipcode(), REGEX_ZIPCODE, 121, 122, response);       
        response = validateValue(register.getPhone(), REGEX_PHONE, 123, 124, response);      
        response = validateValue(register.getUsername(), REGEX_USERNAME, 103, 102, response);      
        response = validateValue(register.getPassword(), REGEX_PASSWORD, 114, 111, response);
        
        if (register.getHouseNumber() <= 0) {
            response.addError(108);
        }
        if (register.getSuffix() != null) {
            if (!register.getSuffix().matches(REGEX_SUFFIX)) {
                response.addError(110);
            }
        } else {
            response.addError(109);
        }
        return response;
    }
}
