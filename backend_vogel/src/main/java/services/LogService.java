/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entity.Log;
import hibernate.HibernateUtil;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author pepijndegoede
 */
public class LogService {
    
    /**
     *
     * @return
     */
    public Session getSession() {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
        } catch (org.hibernate.HibernateException he) {
            session = HibernateUtil.getSessionFactory().openSession();
        }
        return session;
    }
    
    /**
     *
     * @param errorCode
     * @param error
     * @return
     */
    public boolean LogError(String errorCode, String error) {
        
        Session session = getSession();
        
        entity.Log log = new entity.Log();
        Date date = new Date();
        
        try {
            session.getTransaction().begin();
            
            log.setDate(date);
            log.setErrorCode(errorCode);
            log.setError(error); 
            
            session.save(log);
            session.getTransaction().commit();          
        }
        catch (Exception e) {
            return false;
        }
        finally {
            session.close();
        }
        
        return true;         
    }
    
    /**
     *
     * @return
     */
    public List<Log> GetLog() {
        
        Session session = getSession();
        
        List<Log> last = session.createQuery("from Log order by date DESC").setMaxResults(100).list();

        session.close();
        
        return last;
    }
}
