package services;

import entity.Drone;
import entity.Dronesession;
import entity.Spectatorsession;
import entity.User;
import entity.Videofeed;
import hibernate.HibernateUtil;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;
import models.PilotResponse;
import models.PilotSession;
import models.PilotSessionResponse;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import rest.ObjectResponse;

/**
 *
 * @author hrutgers
 */
public class SessionService {

    String pattern = "yyyy-MM-dd HH:mm:ss";
    
    private static final String ENDTIME = "endTime";

    private static final Logger LOG = Logger.getLogger(SessionService.class.getName());

    /**
     *
     */
    public SessionService() {
    }
    
    /**
     * Returns current session or opens a new one
     *
     * @return session Session-object
     */
    public Session getSession() {
        Session session;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
        } catch (org.hibernate.HibernateException he) {
            session = HibernateUtil.getSessionFactory().openSession();
        }
        return session;
    }

    /**
     * Get all pilot sessions ina ObjectResponse-object. In this object you'll
     * find a ArrayList containing PilotSessionResponse-objects.
     *
     * @return response ObjectResponse-object
     */
    public ObjectResponse getPilotSessions() {
        ObjectResponse response = new ObjectResponse();
        Session session = this.getSession();
        try {
            List<PilotSessionResponse> pilotSessions = new ArrayList<>();
            List<Dronesession> droneSessions = session.createCriteria(Dronesession.class)
                    .add(Restrictions.isNotNull(ENDTIME))
                    .list();
            for (Dronesession droneSession : droneSessions) {
                PilotSessionResponse pilotSessionResponse = new PilotSessionResponse(
                        droneSession.getDroneId().getId(),
                        droneSession.getVideoFeedId().getId(),
                        new SimpleDateFormat(pattern).format(droneSession.getStartTime()),
                        droneSession.getEndTime() == null ? null : new SimpleDateFormat(pattern).format(droneSession.getEndTime()),
                        new PilotResponse(
                                droneSession.getPilotId().getId(),
                                droneSession.getPilotId().getName()
                        )
                );
                pilotSessions.add(pilotSessionResponse);
            }
            response.setObject(pilotSessions);
            if (response.noErrors()) {
                response.addError(0);
            }
            return response;
        } catch (Exception e) {
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     * Get all pilot sessions by user-id in a ObjectResponse-object. In this
     * object you'll find a ArrayList containing PilotSessionResponse-objects.
     *
     * @param userId int user-id of a pilot
     * @return response ObjectResponse-object
     */
    public ObjectResponse getPilotSessionsByUserId(int userId) {
        ObjectResponse response = new ObjectResponse();
        Session session = this.getSession();
        try {
            List<Dronesession> droneSessions;
            List<PilotSessionResponse> pilotSessions = new ArrayList<>();
            User user = (User) session.get(entity.User.class, userId);
            if (user == null) {
                response.addError(201);
            } else {
                droneSessions = session.createCriteria(entity.Dronesession.class)
                        .add(Restrictions.eq("pilotId", user))
                        .add(Restrictions.isNotNull(ENDTIME))
                        .list();

                droneSessions.stream().map(droneSession -> new PilotSessionResponse(
                        droneSession.getDroneId().getId(),
                        droneSession.getVideoFeedId().getId(),
                        new SimpleDateFormat(pattern).format(droneSession.getStartTime()),
                        droneSession.getEndTime() == null ? null : new SimpleDateFormat(pattern).format(droneSession.getEndTime())
                )).forEach(pilotSessionResponse -> {
                    pilotSessions.add(pilotSessionResponse);
                });
            }
            response.setObject(pilotSessions);
            if (response.noErrors()) {
                response.addError(0);
            }
            return response;
        } catch (Exception e) {
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     * Open the spectator session
     * @param userId
     * @param droneSessionId
     * @return 
     */
    public ObjectResponse openSpectatorSession(int userId, int droneSessionId) {
        ObjectResponse response = new ObjectResponse();
        Session s = this.getSession();
        Spectatorsession spec = null;

        Dronesession ds = (Dronesession) s.get(Dronesession.class, droneSessionId);
        User u = (User) s.get(User.class, userId);

        if (u != null) {
            if (ds != null) {
                spec = new Spectatorsession();
                try {
                    s.beginTransaction();

                    spec.setDronesessionId(ds);
                    spec.setSpectatorId(u);
                    spec.setStartTime(new Date());

                    s.save(spec);
                    s.getTransaction().commit();

                    response.addError(0); //success
                    response.setObject(spec);
                } catch (Exception e) {
                    throw e;
                } finally {
                    s.close();
                }
            } else {
                response.addError(306); //306 - drone session not found
            }
        } else {
            response.addError(308); //user not found
        }
        return response;
    }

    /**
     * Opening a pilot session. It will check if there is an available drone to
     * pair with the user.
     *
     * @param ps - PilotSession object containing an userId
     * @return response ObjectResponse-object with a list of errors and/or a
     * droneId
     */
    public ObjectResponse openSession(PilotSession ps) {
        ObjectResponse response = new ObjectResponse();
        List<Drone> drones;
        List<Integer> droneIds = new ArrayList<>(), droneSessionIds = new ArrayList<>();
        List<Dronesession> dronesessions;
        Session s = this.getSession();

        Query q = s.getNamedQuery("Drone.findAll");
        drones = (ArrayList<Drone>) q.list();
        drones.stream().forEach(d -> {
            droneIds.add(d.getId());
        });

        dronesessions = s.createCriteria(entity.Dronesession.class)
                .add(Restrictions.isNull(ENDTIME))
                .list();
        
        dronesessions.stream().forEach(d -> {
            droneSessionIds.add(d.getDroneId().getId());
        });

        Collection first = droneIds;
        Collection second = droneSessionIds;
        first.removeAll(second);

        Dronesession droneSession = new Dronesession();
        if (!first.isEmpty()) {
            //pair drone with user
            int droneId = (int) first.toArray()[0];
            int userId = ps.getId();

            User pilot = (User) s.get(User.class, userId);
            Drone drone = (Drone) s.get(Drone.class, droneId);
            Videofeed feed = (Videofeed) s.get(Videofeed.class, 1);
            Date start = new Date();

            s = this.getSession();
            try {
                s.beginTransaction();

                droneSession.setStartTime(start);
                droneSession.setPilotId(pilot);
                droneSession.setDroneId(drone);
                droneSession.setVideoFeedId(feed);

                s.save(droneSession);
                s.getTransaction().commit();

                response.addError(0);
                response.setObject(droneSession);
            } catch (Exception e) {
                throw e;
            } finally {
                s.close();
            }
        } else {
            response.addError(202);
        }
        return response;
    }

    /**
     *
     * @param droneSessionId
     * @return
     */
    public ObjectResponse getDroneSession(int droneSessionId) {
        ObjectResponse response = new ObjectResponse();
        
        Session dbSession = null;
        
        Dronesession droneSession = null;
        
        try {
            dbSession = this.getSession();
            
            droneSession = (Dronesession) dbSession.get(Dronesession.class, droneSessionId);
            
            if (droneSession != null) {
                response.setObject(droneSession);
            } else {
                response.addError(306);
            }
            
        } catch (Exception e) {
            LOG.info(e.toString());
            
        } finally {
            if (dbSession != null && dbSession.isOpen()) {
                dbSession.close();
            }
        }

        if (response.noErrors()) {
            response.addError(0);
        }

        return response;
    }
    
    /**
     *
     * @param droneSessionId
     * @return
     */
    public ObjectResponse closeDroneSession(int droneSessionId) {
        ObjectResponse response = new ObjectResponse();

        Session dbSession = null;
        
        Dronesession droneSession = null;
        
        try {
            dbSession = this.getSession();
            
            droneSession = (Dronesession) dbSession.get(Dronesession.class, droneSessionId);
            
            if (droneSession != null) {
                
                Transaction transaction = dbSession.beginTransaction();
                
                droneSession.setEndTime(new Date());
                
                dbSession.update(droneSession);
                
                transaction.commit();
            } else {
                response.addError(306);
            }
            
        } catch (Exception e) {
            LOG.info(e.toString());
        } finally {
            if (dbSession != null && dbSession.isOpen()) {
                dbSession.close();
            }
        }

        if (response.noErrors()) {
            response.addError(0);
        }

        return response;
    }
}
