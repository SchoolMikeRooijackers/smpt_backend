package services;

import entity.Dronesession;
import entity.FeaturedLocation;
import hibernate.HibernateUtil;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;
import models.FeaturedLocationModel;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import rest.ObjectResponse;

/**
 *
 * @author Yasin
 */
public class FeaturedService {

    // LOG
    private static final Logger LOG = Logger.getLogger(FeaturedService.class.getName());
    /**
     * @return session for hibernate
     */
    private static Session getSession() {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
        } catch (org.hibernate.HibernateException he) {
            session = HibernateUtil.getSessionFactory().openSession();
            LOG.info(he.getMessage());
        }
        return session;
    }

    /**
     *
     * @param model
     * @return
     */
    public ObjectResponse create(FeaturedLocationModel model) {
        ObjectResponse response = new ObjectResponse();

        Session session = null;
        Dronesession droneSession = null;
        try {
            session = getSession();
            droneSession = (Dronesession) session.get(Dronesession.class, model.getDroneSessionId());
            if (droneSession == null) {
                response.addError(204);
                return response;
            } else {
                session.beginTransaction();
                FeaturedLocation featuredLocation = new FeaturedLocation();
                featuredLocation.setTitle(model.getTitle());
                featuredLocation.setSubTitle(model.getSubTitle());
                featuredLocation.setImageUrl(model.getImageUrl());

                session.save(featuredLocation);

                droneSession.setFeaturedLocId(featuredLocation);
                
                session.save(featuredLocation);
                session.getTransaction().commit();
            }
        } catch (Exception e) {
            LOG.info(e.toString());
            if (session != null) {
                session.getTransaction().rollback();
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        response.addError(0);
        return response;
    }

    /**
     *
     * @return
     */
    public ObjectResponse findAll() {
        ObjectResponse response = new ObjectResponse();

        Session session = null;
        try {
            session = getSession();
            Criteria criteria = session.createCriteria(FeaturedLocation.class);
            List<FeaturedLocation> list = (List<FeaturedLocation>)criteria.list();
            response.object = list;
        } catch (Exception e) {
            response.object = new ArrayList<>();
            LOG.info(e.toString());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        response.addError(0);
        return response;
    }

    /**
     *
     * @return
     */
    public ObjectResponse findLive() {
        ObjectResponse response = new ObjectResponse();

        Session session = null;
        try {
            session = getSession();
            List<Dronesession> liveDroneSessions = session.createCriteria(Dronesession.class)
                    .add(Restrictions.isNotNull("featuredLocId"))
                    .add(Restrictions.isNull("endTime"))
                    .list();

            response.object = liveDroneSessions;
        } catch (Exception e) {
            LOG.info(e.toString());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        response.addError(0);
        return response;
    }

    /**
     *
     * @return
     */
    public ObjectResponse findRecent() {
        ObjectResponse response = new ObjectResponse();

        Session session = null;
        try {
            session = getSession();
            List<Dronesession> recentDroneSessions = session.createCriteria(Dronesession.class)
                    .add(Restrictions.isNotNull("featuredLocId"))
                    .addOrder(Order.desc("id"))
                    .setMaxResults(5)
                    .list();

            response.object = recentDroneSessions;
        } catch (Exception e) {
            LOG.info(e.toString());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        response.addError(0);
        return response;
    }

    /**
     *
     * @return
     */
    public ObjectResponse findFeatured() {
        ObjectResponse response = new ObjectResponse();

        Session session = null;
        try {
            session = getSession();
            List<Dronesession> featuredDroneSessions = session.createCriteria(Dronesession.class)
                    .add(Restrictions.isNotNull("featuredLocId"))
                    .list();

            response.object = featuredDroneSessions;
        } catch (Exception e) {
            LOG.info(e.toString());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        response.addError(0);
        return response;
    }
    
    /**
     *
     * @return
     */
    public ObjectResponse all() {
        ObjectResponse response = new ObjectResponse();
        
        // LIST OF ALL FEATURED LOCATIONS
        List<FeaturedLocation> featuredLocations = (List<FeaturedLocation>)findAll().object;
        
        // LIST OF ALL FEATURED LOCATION MODELS
        List<FeaturedLocationModel> featuredLocationModels = new ArrayList<>();
        
        // LOOP THE FEATURED LOCATIONS TO GET THE LIVE/RECENT/FEATURED
        for(FeaturedLocation location : featuredLocations) {
            
            Dronesession ds = null;
            for(Dronesession droneSession : location.getDronesessionList()) {
                ds = droneSession;
                break;
            }
            
            if (ds != null)
            {
                List<Dronesession> live = (List<Dronesession>) findLive().object;
                List<Dronesession> recent = (List<Dronesession>) findRecent().object;
                List<Dronesession> featured = (List<Dronesession>) findFeatured().object;
        
                FeaturedLocationModel featuredLocationModel = new FeaturedLocationModel(location.getTitle(), location.getSubTitle(), location.getImageUrl());
                featuredLocationModel.setDroneSessionId(ds.getId());
                featuredLocationModel.setLive(live);
                featuredLocationModel.setRecent(recent);
                featuredLocationModel.setFeatured(featured);
                
                featuredLocationModels.add(featuredLocationModel);
            }
            
        }
        response.object = featuredLocationModels;
        response.addError(0);
        return response;
    }

}
