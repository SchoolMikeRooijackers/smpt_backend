package rest;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author Yasin
 */
@ApplicationPath("api")
public class ApplicationConfig extends Application {

    /**
     *
     * @return
     */
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new HashSet<>();
        addClasses(resources);
        return resources;
    }

    private void addClasses(Set<Class<?>> resources) {
        // Resources
        resources.add(resource.UserResource.class);
        resources.add(resource.SessionResource.class);
        resources.add(resource.ConnectionResource.class);
        resources.add(resource.FeaturedResource.class);

        // Providers
        resources.add(rest.RestResponseFilter.class);
    }

}
