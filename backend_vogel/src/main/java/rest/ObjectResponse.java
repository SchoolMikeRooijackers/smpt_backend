package rest;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pepijndegoede
 */
public class ObjectResponse {

    private List<Integer> errors;

    /**
     *
     */
    public Object object;

    /**
     *
     */
    public ObjectResponse() {
        errors = new ArrayList<>();
    }

    /**
     *
     * @return
     */
    public List<Integer> getErrors() {
        return errors;
    }

    /**
     *
     * @param errorCode
     */
    public void addError(Integer errorCode) {
        errors.add(errorCode);
    }

    /**
     *
     * @return
     */
    public boolean noErrors() {
        return errors.isEmpty();
    }

    /**
     *
     * @return
     */
    public boolean succes() {
        return errors.contains(0);
    }

    /**
     *
     * @return
     */
    public Object getObject() {
        return object;
    }

    /**
     *
     * @param object
     */
    public void setObject(Object object) {
        this.object = object;
    }

}
