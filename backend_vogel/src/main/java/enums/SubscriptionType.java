package enums;

/**
 *
 * @author mikerooijackers
 */
public enum SubscriptionType {
    FREE,
    PREMIUM,
    UNLIMITED
}
