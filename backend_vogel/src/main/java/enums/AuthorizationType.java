package enums;

/**
 *
 * @author mikerooijackers
 */
public enum AuthorizationType {
    REGISTERED,
    VISITOR
}
