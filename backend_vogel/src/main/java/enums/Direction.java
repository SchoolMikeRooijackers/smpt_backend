package enums;

/**
 *
 * @author mikerooijackers
 */
public enum Direction {
    LEFT,
    RIGHT
}
