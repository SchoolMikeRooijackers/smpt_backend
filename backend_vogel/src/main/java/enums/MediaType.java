package enums;

/**
 *
 * @author mikerooijackers
 */
public enum MediaType {
    VIDEO,
    PHOTO
}
