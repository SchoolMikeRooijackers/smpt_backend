package enums;

/**
 *
 * @author mikerooijackers
 */
public enum UserType {
    PILOT,
    SPECTATOR
}
