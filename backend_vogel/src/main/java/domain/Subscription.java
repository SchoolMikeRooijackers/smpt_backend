package domain;

import enums.SubscriptionType;
import java.util.Date;

/**
 *
 * @author mikerooijackers
 */
public class Subscription {

    private int id;
    private SubscriptionType subscriptionType;
    private String bankAccount;
    private Date startDate;
    private Date endDate;

    /**
     *
     * @param subscriptionType
     * @param bankAccount
     * @param startDate
     * @param endDate
     */
    public Subscription(SubscriptionType subscriptionType, String bankAccount, Date startDate, Date endDate) {
        this.subscriptionType = subscriptionType;
        this.bankAccount = bankAccount;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    /**
     *
     * @return int id
     */
    public int getId() {
        return this.id;
    }

    /**
     *
     * @return SubscriptionType subscriptionType
     */
    public SubscriptionType getSubscriptionType() {
        return subscriptionType;
    }

    /**
     *
     * @param subscriptionType
     */
    public void setSubscriptionType(SubscriptionType subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    /**
     *
     * @return String bankAccount
     */
    public String getBankAccount() {
        return this.bankAccount;
    }

    /**
     *
     * @param bankAccount
     */
    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    /**
     *
     * @return Date startDate
     */
    public Date getStartDate() {
        return this.startDate;
    }

    /**
     *
     * @param startDate
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     *
     * @return Date endDate
     */
    public Date getEndDate() {
        return this.endDate;
    }

    /**
     *
     * @param endDate
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
