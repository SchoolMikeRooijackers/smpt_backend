package domain;

import enums.UserType;
import enums.AuthorizationType;
import java.util.List;

/**
 *
 * @author mikerooijackers
 */
public class User {

    private AuthorizationType authorizationType;
    private UserType userType;
    private String name;
    private String email;
    private String phone;
    private Address address;
    private Character gender;
    private String password;
    private String username;
    private Subscription subscription;
    private List<Vote> votes;

    /**
     *
     * @return AuthorizationType authorizationType
     */
    public AuthorizationType getAuthorizationType() {
        return this.authorizationType;
    }

    /**
     *
     * @param authorizationType
     */
    public void setAuthorizationType(AuthorizationType authorizationType) {
        this.authorizationType = authorizationType;
    }

    /**
     *
     * @return UserType userType
     */
    public UserType getUserType() {
        return this.userType;
    }

    /**
     *
     * @param userType
     */
    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    /**
     *
     * @return String name
     */
    public String getName() {
        return this.name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return String email
     */
    public String getEmail() {
        return this.email;
    }

    /**
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return String phone
     */
    public String getPhone() {
        return this.phone;
    }

    /**
     *
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     *
     * @return Address address
     */
    public Address getAddress() {
        return this.address;
    }

    /**
     *
     * @param address
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     *
     * @return Character gender
     */
    public Character getGender() {
        return this.gender;
    }

    /**
     *
     * @param gender
     */
    public void setGender(Character gender) {
        this.gender = gender;
    }

    /**
     *
     * @return String password
     */
    public String getPassword() {
        return this.password;
    }

    /**
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return String username
     */
    public String getUsername() {
        return this.username;
    }

    /**
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return Subscription subscription
     */
    public Subscription getSubscription() {
        return this.subscription;
    }

    /**
     *
     * @param subscription
     */
    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }

    /**
     *
     * @param vote
     * @return boolean vote
     */
    public boolean addVote(Vote vote) {
        return this.votes.add(vote);
    }

    /**
     *
     * @return List Vote
     */
    public List<Vote> getVotes() {
        return this.votes;
    }
}
