package domain;

/**
 *
 * @author mikerooijackers
 */
public class VideoFeed {

    private int id;
    private String streamURL;
    private String fileLocation;

    /**
     *
     * @param streamURL
     * @param fileLocation
     */
    public VideoFeed(String streamURL, String fileLocation) {
        this.streamURL = streamURL;
        this.fileLocation = fileLocation;
    }

    /**
     *
     * @return int id
     */
    public int getId() {
        return this.id;
    }

    /**
     *
     * @return String streamURL
     */
    public String getStreamURL() {
        return this.streamURL;
    }

    /**
     *
     * @param streamURL
     */
    public void setStreamURL(String streamURL) {
        this.streamURL = streamURL;
    }

    /**
     *
     * @return String fileLocation
     */
    public String getFileLocation() {
        return this.fileLocation;
    }

    /**
     *
     * @param fileLocation
     */
    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }
}
