package domain;

/**
 *
 * @author mikerooijackers
 */
public class Location {

    private int id;
    private float latitude;
    private float longitude;
    private float altitude;

    /**
     *
     */
    public Location() {
        id = 0;
    }

    /**
     *
     * @param id
     * @param latitude
     * @param longitude
     * @param altitude
     */
    public Location(int id, float latitude, float longitude, float altitude) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return int id
     */
    public int getId() {
        return this.id;
    }

    /**
     *
     * @return float latitude
     */
    public float getLatitude() {
        return this.latitude;
    }

    /**
     *
     * @param latitude
     */
    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    /**
     *
     * @return float longitude
     */
    public float getLongitude() {
        return this.longitude;
    }

    /**
     *
     * @param longitude
     */
    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    /**
     *
     * @return float altitude
     */
    public float getAltitude() {
        return this.altitude;
    }

    /**
     *
     * @param altitude
     */
    public void setAltitude(float altitude) {
        this.altitude = altitude;
    }
}
