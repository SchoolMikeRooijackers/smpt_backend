package domain;

import java.util.Date;

/**
 *
 * @author mikerooijackers
 */
public class DroneSession {

    private Drone drone;
    private VideoFeed videoFeed;
    private Date startTime;
    private Date endTime;

    /**
     *
     * @param drone
     * @param videoFeed
     * @param startTime
     * @param endTime
     */
    public DroneSession(Drone drone, VideoFeed videoFeed, Date startTime, Date endTime) {
        this.drone = drone;
        this.videoFeed = videoFeed;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    /**
     *
     * @return Drone drone
     */
    public Drone getDrone() {
        return this.drone;
    }

    /**
     *
     * @param drone
     */
    public void setDrone(Drone drone) {
        this.drone = drone;
    }

    /**
     *
     * @return VideoFeed videoFeed
     */
    public VideoFeed getVideoFeed() {
        return this.videoFeed;
    }

    /**
     *
     * @param videoFeed
     */
    public void setVideoFeed(VideoFeed videoFeed) {
        this.videoFeed = videoFeed;
    }

    /**
     *
     * @return Date startTime
     */
    public Date getStartTime() {
        return this.startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     *
     * @return
     */
    public Date getEndTime() {
        return this.endTime;
    }

    /**
     *
     * @param endTime
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}
