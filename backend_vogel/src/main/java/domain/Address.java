package domain;

/**
 *
 * @author mikerooijackers
 */
public class Address {

    private final int id;
    private String street;
    private String houseNumber;
    private String zipcode;
    private String city;
    private String country;

    /**
     *
     * @param id
     * @param street
     * @param houseNumber
     * @param zipcode
     * @param city
     * @param country
     */
    public Address(int id, String street, String houseNumber, String zipcode, String city, String country) {
        this.id = id;
        this.street = street;
        this.houseNumber = houseNumber;
        this.zipcode = zipcode;
        this.city = city;
        this.country = country;
    }

    /**
     *
     * @return int id
     */
    public int getId() {
        return this.id;
    }

    /**
     *
     * @return string street
     */
    public String getStreet() {
        return this.street;
    }

    /**
     *
     * @param street
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     *
     * @return string house number
     */
    public String getHouseNumber() {
        return this.houseNumber;
    }

    /**
     *
     * @param houseNumber
     */
    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    /**
     *
     * @return string zipcode
     */
    public String getZipcode() {
        return this.zipcode;
    }

    /**
     *
     * @param zipcode
     */
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    /**
     *
     * @return string city
     */
    public String getCity() {
        return this.city;
    }

    /**
     *
     * @param city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     *
     * @return string country
     */
    public String getCountry() {
        return this.country;
    }

    /**
     *
     * @param country
     */
    public void setCountry(String country) {
        this.country = country;
    }
}
