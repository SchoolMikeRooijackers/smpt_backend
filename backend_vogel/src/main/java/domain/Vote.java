package domain;

/**
 *
 * @author mikerooijackers
 */
public class Vote {

    private int id;
    private int rating;

    /**
     *
     * @param rating
     */
    public Vote(int rating) {
        this.rating = rating;
    }

    /**
     *
     * @return int id
     */
    public int getId() {
        return this.id;
    }

    /**
     *
     * @return int rating
     */
    public int getRating() {
        return this.rating;
    }

    /**
     *
     * @param rating
     */
    public void setRating(int rating) {
        this.rating = rating;
    }
}
