package domain;

import java.util.Date;

/**
 *
 * @author mikerooijackers
 */
public class UserSession {

    private int id;
    private Date startTime;
    private Date endTime;

    /**
     *
     * @param startTime
     * @param endTime
     */
    public UserSession(Date startTime, Date endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    /**
     *
     * @return int id
     */
    public int getId() {
        return this.id;
    }

    /**
     *
     * @return Date startTime
     */
    public Date getStartTime() {
        return this.startTime;
    }

    /**
     *
     * @param startTime
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     *
     * @return Date endTime
     */
    public Date getEndTime() {
        return this.endTime;
    }

    /**
     *
     * @param endTime
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}
