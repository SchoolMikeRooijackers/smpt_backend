package domain;

/**
 *
 * @author mikerooijackers
 */
public class Drone {

    private final int id;
    private Location currentLocation;
    private Location baseLocation;

    /**
     *
     * @param id
     * @param currentLocation
     * @param baseLocation
     */
    public Drone(int id, Location currentLocation, Location baseLocation) {
        this.id = id;
        this.currentLocation = currentLocation;
        this.baseLocation = baseLocation;
    }

    /**
     *
     * @return int id
     */
    public int getId() {
        return this.id;
    }

    /**
     *
     * @return Location currentLocation
     */
    public Location getCurrenLocation() {
        return this.currentLocation;
    }

    /**
     *
     * @param currentLocation
     */
    public void setCurrenLocation(Location currentLocation) {
        this.currentLocation = currentLocation;
    }

    /**
     *
     * @return Location baseLocation
     */
    public Location getBaseLocation() {
        return this.baseLocation;
    }

    /**
     *
     * @param baseLocation
     */
    public void setBaseLocation(Location baseLocation) {
        this.baseLocation = baseLocation;
    }
}
