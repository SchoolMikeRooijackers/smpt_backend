package domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mikerooijackers
 */
public class Media {

    private final int id;
    private String fileLocation;
    private int length;
    private Location location;
    private List<Vote> votes = null;

    /**
     *
     * @param id
     * @param fileLocation
     * @param length
     * @param location
     */
    public Media(int id, String fileLocation, int length, Location location) {
        this.id = id;
        this.fileLocation = fileLocation;
        this.length = length;
        this.location = location;
        this.votes = new ArrayList<>();
    }

    /**
     *
     * @return int id
     */
    public int getId() {
        return this.id;
    }

    /**
     *
     * @return String fileLocation
     */
    public String getFileLocation() {
        return this.fileLocation;
    }

    /**
     *
     * @param fileLocation
     */
    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    /**
     *
     * @return int length
     */
    public int getLength() {
        return this.length;
    }

    /**
     *
     * @param length
     */
    public void setLength(int length) {
        this.length = length;
    }

    /**
     *
     * @return Location location
     */
    public Location getLocation() {
        return this.location;
    }

    /**
     *
     * @param location
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     *
     * @param vote
     * @return boolean vote
     */
    public boolean addVote(Vote vote) {
        return this.votes.add(vote);
    }
}
